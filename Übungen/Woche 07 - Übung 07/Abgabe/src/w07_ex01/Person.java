package w07_ex01;

public class Person {
    private String firstName;
    private String lastName;

    public Person() {
        firstName = "";
        lastName = "";
    }

    public Person(String firstname) {
        this.firstName = firstname;
    }

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(firstName);
        if (lastName != null)
            builder.append(" ").append(lastName);
        return builder.toString();
    }

}
