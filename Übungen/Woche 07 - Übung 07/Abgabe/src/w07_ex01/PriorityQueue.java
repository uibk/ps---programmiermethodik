package w07_ex01;

import java.util.*;

public class PriorityQueue<T> {

    private TreeMap<Integer, ArrayDeque<T>> treeMap;

    public PriorityQueue() {
        this.treeMap = new TreeMap<>((int1, int2) -> Integer.compare(int2, int1));
    }

    public void add(T object, int priority) {
        // If same priority is already stored, add it to queue from existing entry
        if (treeMap.containsKey(priority)) {
            treeMap.get(priority).add(object);
        } else {
            ArrayDeque<T> deque = new ArrayDeque<>();
            deque.add(object);
            treeMap.put(priority, deque);
        }
    }

    public void next() {
        ArrayDeque<T> queue = treeMap.firstEntry().getValue();
        if (queue.size() == 1) {
            treeMap.remove(treeMap.firstKey());
        }
        queue.poll();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        treeMap.forEach((key, value) -> value.forEach((entry) -> stringBuilder.append(entry).append(", ")));
        stringBuilder.append("]");
        return stringBuilder.toString();
    }
}
