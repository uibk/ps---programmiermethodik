package w07_ex01;

import java.util.ArrayList;

public class PriorityTest {

    public static void main(String[] args) {
        PriorityQueue<Person> personQueue = new PriorityQueue<>();
        personQueue.add(new Person("Hans"), 5);
        personQueue.add(new Person("Peter"), 3);
        personQueue.add(new Person("Birgit"), 10);
        personQueue.add(new Person("Sarah"), 2);
        personQueue.add(new Person("Herbert"), 6);

        System.out.println(personQueue.toString());

        personQueue.add(new Person("Sofia"), 1);

        personQueue.next();
        personQueue.next();

        System.out.println(personQueue.toString());

        ArrayList<Person> personList = new ArrayList<>();
        personList.add(new Person("Rebekka"));
        personList.add(new Person("Boris"));
        personList.add(new Person("Tiziana"));
        personList.add(new Person("Livio"));

        personQueue.add(personList.get(0), 3);
        personQueue.add(personList.get(1), 5);
        personQueue.add(personList.get(2), 3);
        personQueue.add(personList.get(3), 3);

        personQueue.next();
        System.out.println(personQueue.toString());

    }
}
