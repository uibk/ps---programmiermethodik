package w07_ex02;

import java.util.Iterator;
import java.util.LinkedList;

public class Route<T extends Coordinate> implements Comparable<Route<T>> {

    private LinkedList<T> routeList = new LinkedList<>();
    private double routeCost;

    public Route(T endPoint, double routeCost) {
        this.routeList.add(endPoint);
        this.routeCost = routeCost;
    }

    public Route(Route<T> routeList, T coordinate, double routeCost) {
        this.add(routeList);
        this.routeList.add(coordinate);
        this.routeCost = routeCost;
    }

    public T getFirst() {
        return this.routeList.get(0);
    }

    public T getLast() {
        return this.routeList.get(this.routeList.size() - 1);
    }

    public double getRouteCost() {
        return routeCost;
    }

    public void setRouteCost(double routeCost) {
        this.routeCost = routeCost;
    }

    public void add(T point) {
        this.routeList.add(0, point);
    }

    public void add(Route<T> route) {
        Iterator<T> iterator = route.routeList.descendingIterator();
        while (iterator.hasNext()) {
            add(iterator.next());
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Shortest connection from ");
        builder.append(this.getFirst().toString());
        builder.append(" to ");
        builder.append(this.getLast().toString());
        builder.append(" has a total cost of ");
        builder.append(this.getRouteCost());
        builder.append(" and takes the following route:\n");

        for (T point : this.routeList) {
            builder.append(point.toString());
            builder.append(" -> ");
        }
        builder.delete(builder.length() - 5, builder.length());
        builder.append("\n");
        return builder.toString();
    }

    @Override
    public int compareTo(Route<T> o) {
        return Double.compare(this.routeCost, o.routeCost);
    }
}

