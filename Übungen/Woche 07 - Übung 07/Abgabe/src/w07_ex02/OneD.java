package w07_ex02;

import java.util.Objects;

public class OneD extends Coordinate {
    private double x;

    public OneD(double x) {
        this.x = x;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(x);

        return stringBuilder.toString();
    }

    public double getX() {
        return this.x;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof OneD) {
            return this.x == ((OneD) other).getX();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x);
    }
}

