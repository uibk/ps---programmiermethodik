package w07_ex02;

public abstract class Coordinate {

    @Override
    public abstract String toString();

    @Override
    public abstract boolean equals(Object object);

    @Override
    public abstract int hashCode();

}
