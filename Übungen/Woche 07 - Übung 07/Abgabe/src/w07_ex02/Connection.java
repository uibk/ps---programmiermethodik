package w07_ex02;

public class Connection<T extends Coordinate> implements Comparable<Connection<T>> {

    private T firstPoint;
    private T secondPoint;
    private double travelCost;

    public Connection(T firstPoint, T secondPoint, double travelCost) {
        this.firstPoint = firstPoint;
        this.secondPoint = secondPoint;
        this.travelCost = travelCost;
    }

    public T getFirstPoint() {
        return firstPoint;
    }

    public T getSecondPoint() {
        return secondPoint;
    }

    public double getTravelCost() {
        return travelCost;
    }

    @Override
    public int compareTo(Connection<T> tConnection) {
        return Double.compare(travelCost, tConnection.travelCost);
    }
}
