package w07_ex02;

import java.util.Objects;

public class TwoD extends Coordinate {
    private double x;
    private double y;

    public TwoD(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("X:").append(x);
        stringBuilder.append(" ");
        stringBuilder.append("Y:").append(y);

        return stringBuilder.toString();
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof TwoD) {
            return this.x == ((TwoD) other).getX() && this.y == ((TwoD) other).getY();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x)*Objects.hash(y);
    }
}
