package w07_ex02;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class PathFinder<T extends Coordinate> {

    private List<Connection<T>> connectionList;
    private List<Route<T>> listOfRoutes = new LinkedList<>();

    public PathFinder(List<Connection<T>> connectionList) {
        this.connectionList = connectionList;
        initializeRouteList();
    }

    private void initializeRouteList() {
        for (Connection<T> connection : this.connectionList) {
            Route newRoute = new Route(connection.getFirstPoint(), Double.POSITIVE_INFINITY);
            if (!this.listOfRoutes.contains(newRoute)) {
                this.listOfRoutes.add(newRoute);
            }

            newRoute = new Route(connection.getSecondPoint(), Double.POSITIVE_INFINITY);
            if (!this.listOfRoutes.contains(newRoute)) {
                this.listOfRoutes.add(newRoute);
            }
        }
    }

    private List<Connection<T>> toNeighbours(T coordinate) {
        List<Connection<T>> neighbourList = new ArrayList<>();
        for (Connection<T> toNeighbour : this.connectionList) {
            if (coordinate.equals(toNeighbour.getFirstPoint())) {
                neighbourList.add(toNeighbour);
            }
            if (coordinate.equals(toNeighbour.getSecondPoint())) {
                neighbourList.add(new Connection<>(toNeighbour.getSecondPoint(), toNeighbour.getFirstPoint(), toNeighbour.getTravelCost()));
            }
        }
        return neighbourList;
    }

    public Route<T> getShortestRoute(T startPoint, T endPoint) {
        int startIndex;
        if ((startIndex = listOfRoutes.indexOf(new Route<>(startPoint, 0))) < 0)
            throw new IllegalArgumentException("startPoint is not in given graph");

        listOfRoutes.get(startIndex).setRouteCost(0);

        while (!listOfRoutes.isEmpty()) {

            Collections.sort(this.listOfRoutes);

            // remove first (smallest) element
            Route<T> currentRoute = listOfRoutes.remove(0);

            // if we get to our last-point we have found our shortest track
            if (currentRoute.getLast().equals(endPoint))
                return currentRoute;

            // get neighbours of current route-tail
            List<Connection<T>> toNeighbours = toNeighbours(currentRoute.getLast());

            // for every neighbour (if it wasnt visited before)
            for (Connection<T> toNeighbour : toNeighbours) {
                if (listOfRoutes.contains(new Route<>(toNeighbour.getSecondPoint(), 0))) {

                    //calculate lengths of currentRoute and possible neighbours
                    double alternativeLength = currentRoute.getRouteCost() + toNeighbour.getTravelCost();

                    //could lead to (index < 0) element not in list, but than neighbour-method has a faulty implementation
                    int neighbourIndex = this.listOfRoutes.indexOf(new Route<>(toNeighbour.getSecondPoint(), 0));

                    //If alt-length < length of neighbour
                    if (Double.compare(alternativeLength, this.listOfRoutes.get(neighbourIndex).getRouteCost()) < 0) {

                        //calling special constructor of Route: if the currentRoute to our neighbour is shorter than any previous route to our neighbour
                        //we create a new route containing our previous route and the neighbour's index, with the alternative length, so
                        //this is a smaller route than other routes
                        Route<T> betterRoute = new Route<>(currentRoute, this.listOfRoutes.get(neighbourIndex).getLast(), alternativeLength);

                        //removing old route
                        this.listOfRoutes.remove(neighbourIndex);
                        //adding new route
                        this.listOfRoutes.add(betterRoute);
                    }
                }
            }
        }
        return null;
    }

}

