### Exercise 03

###### a)
**Type Erasure:**
- If you use mechanics from Java like generics, there will be a background process (executed by the compiler) called type erasure.
- it simply deletes all generic types and substitutes into default type-bounds like:

```Java
public class Test<T> {
    private T firstElement;
    private T secondElement;
    
    public Test(T firstElement, T secondElement) {
        this.firstElement = firstElement;
        this.secondElement = secondElement;
    }
}
``` 
into:
```Java
public class Test {
    private Object firstElement;
    private Object secondElement;
    
    public Test(Object firstElement, Object secondElement) {
        this.firstElement = firstElement;
        this.secondElement = secondElement;
    }
}
``` 

###### b)

**Restrictions:**

Consider following situation:

```Java
class Pair<K, V> {

    private K key;
    private V value;

    public Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    // ...
}
``` 

- when instantiating a Pair object it is not allowed to use primitive types for generic substitution like:
```Java
Pair<int, char> p = new Pair<>(8, 'a');
```
It would gives us a compile-error. It is only allowed to use non-primitive Types as Generics works in a way of Object type-substitution.

- You cannot create instances of generic type parameters

This is not allowed:
```Java
public static <E> void append(List<E> list) {
    E elem = new E();  
    list.add(elem);
}
``` 

- Cannot use static data fields as type from type parameters
```Java
public class CarRental<T> {
    private static T car;
}
```

Considering following situation would confuse programmers as well as the compiler itself:

```Java
CarRental<Audi> audi = new CarRental();
CarRental<BMW> bmw = new CarRental();
CarRental<Porsche> porsche = new CarRental();
```

The static field would be shared over all three classes Audi, BMW and Porsche. It is undefined how the type of the static data field is finally determined.

- Cannot use instanceof with parameterized types

This occurs in a compile-time error:
```Java
public <E> addList(List<E> list) {
    if (list instanceof ArrayList<Integer>) {
        // ...
    }
}
```
At compile-time we cannot assure that list is of type ArrayList containing Integer's as type erasure deletes parameter types and substitute it with default type-bounds.

- Cannot create arrays of parameter types

```Java
Object[] strings = new String[2];
strings[0] = "hi";
strings[1] = 100; 
```
With the last line inserted it won't compile because we are storing an other Type than expected String in it.

Same thing here:
```Java
// normally not working, but we are pretending it's working
Object[] stringLists = new List<String>[]; 
// working
stringLists[0] = new ArrayList<String>(); 
// not working -> ArrayStoreException is thrown but runtime cannot detect it
stringLists[1] = new ArrayList<Integer>(); 
```

###### c)

**Diamond Operator**

- The diamond operator acts for simplicity. As the statement in older versions of Java would be like 

```Java
List<Integer> list = new ArrayList<Integer>();
```

Since Java 7 we can use it like that:

```Java
List<Integer> list = new ArrayList<>();
```
The purpose and technical background behind this is simply that the compiler will infer the type proposed in the first statement *List<Integer> list* and can therefore guess what the dynamic type of this specific variable is.

**var keyword**

In fact, it is possible to use a combination of the *var* keyword and diamond operator.

From logical conclusion I would say that at runtime it will infer the real type of the local variable. This statement shows an example:

```Java
public void add(Integer value) {
    var list = new ArrayList<>();
    // Infer to object type from right hand-side from declaration above 
    list.add(value);
}
```
After research I read a post saying that the compiler always inferring the type of the right hand-side. 
Therefore, the example shown above would infer the type *ArrayList<Object>*.