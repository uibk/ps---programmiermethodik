package w02_ex01;

import w02_ex02.Group;

/**
 * This class represents a student in the course management system.
 */
public class Student {
    private int studentId;
    private String firstName;
    private String lastName;
    private Group group;

    /**
     * Creates an empty student object.
     */
    public Student() {
    }

    /**
     * Creates a student object.
     *
     * @param studentId the student id assigned for the created student.
     */
    public Student(int studentId) {
        this.studentId = studentId;
    }

    /**
     * Creates a student object.
     *
     * @param firstName the first name of the student.
     * @param lastName  the last name of the student.
     */
    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * Creates a student object.
     *
     * @param studentId the id of the student.
     * @param firstName the first name of the student.
     * @param lastName  the last name of the student.
     */
    public Student(int studentId, String firstName, String lastName) {
        this.studentId = studentId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * Gets the id of a student.
     *
     * @return the id of the student.
     */
    public int getStudentId() {
        return studentId;
    }

    /**
     * Sets the id of the student.
     *
     * @param studentId the id to set for the student.
     */
    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    /**
     * Gets the first name of the student.
     *
     * @return the first name of the student.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the first name of the leaer.
     *
     * @param firstName the first name of the student.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the last name of the student.
     *
     * @return the last name of the student.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the last name of the student.
     *
     * @param lastName the last name of the student.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentId=" + studentId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", group=" + group +
                '}';
    }
}
