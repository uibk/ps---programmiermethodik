package w02_ex01;

import java.util.ArrayList;

public class ListTest {
    public static void main(String[] args) {
        // Creates an ArrayList object called listOfCourses that stores Strings.
        ArrayList<String> listOfCourses = new ArrayList<>();
        // To add elements to the ArrayList, use the add() method:
        listOfCourses.add("Discrete Mathematics");
        listOfCourses.add("Data Base Management");
        listOfCourses.add("Operating Systems");
        /*
         * To access an element in the ArrayList, use the get() method and refer to
         * the index number:
         */
        System.out.println("print the course with index 1: " + listOfCourses.get(1));
        // Should print "Data Base Management", as counting starts at 0

        // To modify an element, use the set() method and refer to the index.
        listOfCourses.set(0, "Programming Methodology");

        // To remove an element, use the remove() method and refer to the index.
        listOfCourses.remove(0);

        // You can loop through an ArrayList with the for-each loop:
        for (String course : listOfCourses) {
            System.out.println("Course: " + course);
            // Should print all courses from the listOfCourses
        }

        ArrayList<Student> listOfStudents = new ArrayList<>();
        Student student1 = new Student("John", "Doe");
        Student student2 = new Student(1, "Max", "Mustermann");
        Student student3 = new Student(2, "Sabine", "Musterfrau");

        listOfStudents.add(student1);
        listOfStudents.add(student2);
        listOfStudents.add(student3);

        for (Student student : listOfStudents) {
            System.out.println("Student: " + student.getFirstName() + " " + student.getLastName());
        }
    }
}
