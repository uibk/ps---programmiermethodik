package w02_ex01;

public class LeaderManagementTest {

    public static void main(String[] args) {

        Leader firstLeader = new Leader();
        Leader secondLeader = new Leader("John", "Doe");
        Leader thirdLeader = new Leader(1, "Max", "Mustermann", "00415563325", 2765.55);

        firstLeader.printFullName();
        secondLeader.printFullName();
        thirdLeader.printFullName();
    }
}
