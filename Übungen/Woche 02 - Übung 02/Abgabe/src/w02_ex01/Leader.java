package w02_ex01;

public class Leader {
    private int leaderId;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private double salary;

    Leader() {
    }

    Leader(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    Leader(int leaderId, String firstName, String lastName, String phoneNumber, double salary) {
        this.leaderId = leaderId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.salary = salary;
    }

    public int getLeaderId() {
        return leaderId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public double getSalary() {
        return salary;
    }

    public void printFullName() {
        System.out.println("Full name: " + this.getFirstName() + " " + this.getLastName());
    }

    public double calculateSalary(int numberOfMonths) {
        return this.salary * numberOfMonths;
    }
}
