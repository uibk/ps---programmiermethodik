package w02_ex02;

import w02_ex01.Student;

public class GroupManagementTest {

    public static void main(String[] args) {

        // TODO: [Exercise 2.c] Create at least 5 students using the constructor: Student(int studentId,
        // String firstName, String lastName).
        Student student1 = new Student(1, "John", "Doe");
        Student student2 = new Student(2, "Max", "Mustermann");
        Student student3 = new Student(3, "Sabine", "Musterfrau");
        Student student4 = new Student(4, "Gabriel", "Torvalds");
        Student student5 = new Student(5, "Bill", "Gates");

        // TODO: [Exercise 2.c] Create at least 1 group with its attributes. Use the method Group(int
        // groupId, int capacity).
        Group group = new Group(1, 10);
        Group group2 = new Group(2, 10);
        // TODO: [Exercise 2.c] Add created students to the ListOfStudents
        group.addStudent(student1);
        group.addStudent(student2);
        group.addStudent(student3);
        group.addStudent(student4);
        group2.addStudent(student5);
        // TODO: [Exercise 2.c] Remove at least 1 student from the listOfStudents
        group.removeStudent(student2);
        // TODO: [Exercise 2.c] Print out the index of all students from the listOfStudents using
        for (Student student :
                group.getListOfStudents()) {
            System.out.println("Index: " + group.getStudentIndex(student) + " Name: " + student.getFirstName());
        }
        // TODO: [Exercise 3.b] Print out the group belong to a student
        for (Student student :
                group.getListOfStudents()) {
            System.out.println("Name: " + student.getFirstName() + " - Group Id: " + student.getGroup().getGroupId());
        }
        // TODO: [Exercise 3.b] Print Group
        group.print();
        group2.print();
    }
}
