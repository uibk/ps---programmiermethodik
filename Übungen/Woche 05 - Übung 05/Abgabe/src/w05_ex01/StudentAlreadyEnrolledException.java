package w05_ex01;

public class StudentAlreadyEnrolledException extends Exception {
    public StudentAlreadyEnrolledException() {
    }

    public StudentAlreadyEnrolledException(String message) {
        super(message);
    }
}
