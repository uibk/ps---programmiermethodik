package w05_ex01;

public class CourseFullException extends Exception {

    public CourseFullException() {
    }

    public CourseFullException(String message) {
        super(message);
    }
}

