package w05_ex01;

import java.util.ArrayList;
import java.util.List;

public class Course {

    private static final int MAX_STUDENTS = 2;
    private List<String> students = new ArrayList<>();

    public static void main(String[] args) {
        Course course = new Course();
        List<String> students = new ArrayList<>();
        students.add("Donald Duck");
        students.add("Donald Duck");
        students.add("Uncle Scrooge");
        students.add("Gyro Gearloose");

        for (String student : students) {
            try {
                course.addStudent(student);
                System.out.println("Successfully added " + student);
            } catch (CourseFullException | StudentAlreadyEnrolledException e) {
                System.out.println("Failed to add Student: " + student + " " + e.toString());
            }
        }

    }

    public boolean addStudent(String student) throws CourseFullException, StudentAlreadyEnrolledException {
        if (students.size() >= MAX_STUDENTS) {
            throw new CourseFullException("Course if full!");
        }
        if (students.contains(student)) {
            throw new StudentAlreadyEnrolledException("Student already enrolled!");
        }
        students.add(student);
        return true;
    }
}
