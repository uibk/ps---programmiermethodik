package w05_ex02;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class BasicProductTest {

    @Test
    void nullName() {
        assertThrows(IllegalArgumentException.class, () -> new BasicProduct(null, 0));
    }

    @Test
    void negativePrice() {
        assertThrows(IllegalArgumentException.class, () -> new BasicProduct(null, -100));
    }

    @Test
    void getName() {
        assertEquals("Chair", new BasicProduct("Chair", 100).getName());
    }

    @Test
    void getPrice() {
        assertEquals(100, new BasicProduct("Chair", 100).getPrice());
    }
}