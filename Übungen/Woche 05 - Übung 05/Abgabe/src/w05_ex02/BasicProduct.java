package w05_ex02;

public class BasicProduct implements Product {
    private String name;
    private double price;

    public BasicProduct(String name, double price) throws IllegalArgumentException {
        if (name == null || price <= 0) {
            throw new IllegalArgumentException("Given values must be valid!");
        }
        this.name = name;
        this.price = price;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }
}
