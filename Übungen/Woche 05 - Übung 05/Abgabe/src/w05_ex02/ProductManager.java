package w05_ex02;

import java.util.LinkedList;
import java.util.List;

public class ProductManager {
    private int size;
    private int maxSize;
    private List<Product> products;
    private double totalPrice;

    public ProductManager(int maxSize) throws IllegalArgumentException {
        if (maxSize < 0) {
            throw new IllegalArgumentException("Maximum Size cannot be less than 0!");
        }
        this.maxSize = maxSize;
        this.products = new LinkedList<>();
    }

    public void addProduct(Product product) throws StoreFullException {
        if (size >= maxSize) {
            throw new StoreFullException("List of products is full!");
        }
        size = products.size();
        checkDistinction(product);
        products.add(product);
    }

    private void checkDistinction(Product product) {
        if (!products.isEmpty()) {
            products.forEach((storedProduct -> {
                if (!product.getName().equals(storedProduct.getName())) {
                    size++;
                }
            }));
        }
    }

    public void removeProduct(Product product) {
        products.remove(product);
    }

    public double getTotalPrice() {
        products.forEach((product -> totalPrice += product.getPrice()));
        return totalPrice;
    }

    public List<Product> getProducts() {
        return products;
    }

    public int getSize() {
        return size;
    }

    public int getMaxSize() {
        return maxSize;
    }
}
