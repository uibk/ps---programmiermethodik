package w09_ex03.sheet01;

/**
 * This class is used for calculating a possible discount for a shopping cart.
 */
public class ShoppingCart {

    /**
     * Main method of the program.
     *
     * @param args command line arguments.
     */
    public static void main(final String[] args) {

        // shopping cart
        double[] shoppingCart1 = new double[]{40.95, 10.80, 2.35, 22.50, 13.45, 5.95, 33.33, 41.10, 7.75, 5};
        double[] currentCart = shoppingCart1;

        int numberOfItems = currentCart.length;
        double originalPrice = totalPrice(currentCart);
        double discountedPrice = calculateDiscountedPrice(originalPrice, numberOfItems);

        // ******OUTPUT on console:******
        System.out.println(" Original price:   " + originalPrice);
        System.out.println(" Discounted price: " + discountedPrice);
    }

    /**
     * Calculates the discounted price for a shopping cart based on the number of
     * items in the cart and the discount amount.
     *
     * @param originalPrice the price of the cart without discount.
     * @param numberOfItems the number of items in the cart.
     * @return The discounted price of the shopping cart.
     */
    private static double calculateDiscountedPrice(final double originalPrice, final int numberOfItems) {
        double discountAmount = calculateDiscount(originalPrice);
        double discount;

        // use switch statement to accomplish cases of discount's (5, 10, 15, 20, 25, 30 items)
        switch (numberOfItems) {
            case 5:
            case 10:
            case 15:
            case 20:
            case 25:
            case 30:
                discount = originalPrice * (discountAmount / 100) + numberOfItems;
                break;
            default:
                return originalPrice;
        }
        return Math.round((originalPrice - discount) * 100) / 100.0d;
    }

    /**
     * Calculates the discount amount depending on original price of the cart.
     *
     * @param originalPrice the original total price of a cart.
     * @return The discount that one gets on the original price.
     */
    private static double calculateDiscount(final double originalPrice) {
        if (originalPrice < 50) {
            return 0;
        }
        if (originalPrice >= 50 && originalPrice < 60) {
            return 5;
        }

        if (originalPrice >= 60 && originalPrice < 70) {
            return 10;
        }

        if (originalPrice >= 70 && originalPrice < 100) {
            return 15;
        }

        if (originalPrice >= 100) {
            return 20;
        }
        return 0;
    }

    /**
     * Calculates the total price of all product contained in a cart.
     *
     * @param cart the shopping cart to calculate the price for.
     * @return The summ of all prices in a cart.
     */
    private static double totalPrice(final double[] cart) {
        double sum = 0.0;
        for (double price : cart) {
            sum += price;
        }

        return Math.round(sum * 100) / 100.0d;
    }
}
