package w09_ex03.sheet02;

import w09_ex03.sheet02.dependencies.Leader;
import w09_ex03.sheet02.dependencies.Student;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * This class represents a group in the course management system.
 */
public class Group {
    private int groupId;
    private int capacity;
    private Leader leader;
    private List<Student> listOfStudents;

    /**
     * Creates a group object.
     *
     * @param groupId  the id assigned to the group.
     * @param capacity the maximum capacity of the group.
     */
    public Group(int groupId, int capacity, List<Student> list) {
        this.groupId = groupId;
        this.capacity = capacity;
        this.listOfStudents = new ArrayList<>(capacity);
    }

    /**
     * Adds a student to the group.
     *
     * @param student the student that should be added.
     * @return True, if the student was successfully added, otherwise false.
     */
    public boolean addStudent(Student student) {
        if (this.listOfStudents.size() >= this.capacity) {
            System.err.println("The student could not be added.");
            return false;
        } else {
            student.setGroup(this);
            listOfStudents.add(student);
            return true;
        }
    }

    /**
     * Removes a student from the group.
     *
     * @param student the student that should be removed.
     * @return true, if the studend was successfully removed, otherwise false.
     */
    public boolean removeStudent(Student student) {
        student.setGroup(null);
        return listOfStudents.remove(student);
    }

    /**
     * Gets the index of a student in the group.
     *
     * @param student the student to search the index for.
     * @return -1, if the student is not in the group, otherwise the respective
     * index.
     */
    public int getStudentIndex(Student student) {
        return listOfStudents.indexOf(student);
    }

    /**
     * Prints the group on the standart output.
     */
    public void print() {
        System.out.println();
        System.out.println("groupId - studentId firstName lastName");
        for (Student student :
                this.getListOfStudents()) {
            System.out.println(student.getGroup().getGroupId() + " - " + student.getStudentId() + " " + student.getFirstName() + " " + student.getLastName());
        }
    }

    /**
     * Gets the id of the group.
     *
     * @return the id of the group.
     */
    public int getGroupId() {
        return groupId;
    }

    /**
     * Sets the id of the group.
     *
     * @param groupId the id to set for the group.
     */
    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    /**
     * Gets the maximum capacity of the group.
     *
     * @return the maximum capacity of the group.
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * Sets the capaity of the group to the given capacity if there are less
     * students than capacity in the list, otherwise it sets the capacity to the
     * number of students in the group.
     *
     * @param capacity the maximum capacity to set for the group.
     */
    public void setCapacity(int capacity) {
        if (listOfStudents.size() <= capacity) {
            this.capacity = capacity;
        } else {
            this.capacity = listOfStudents.size();
        }
    }

    /**
     * Gets the leader of the group.
     *
     * @return the leader of the group.
     */
    public Leader getLeader() {
        return leader;
    }

    /**
     * Sets the leader of the group.
     *
     * @param leader the leader to set for the group.
     */
    public void setLeader(Leader leader) {
        this.leader = leader;
    }

    /**
     * Gets the list of students in a group.
     *
     * @return an unmodifiable list of students in the group.
     */
    public List<Student> getListOfStudents() {
        return Collections.unmodifiableList(listOfStudents);
    }
}