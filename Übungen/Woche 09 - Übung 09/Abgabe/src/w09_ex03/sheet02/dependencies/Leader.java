package w09_ex03.sheet02.dependencies;

/**
 * This class represents a leader in the course management system.
 */
public class Leader {
	// [Exercise 1.a] Create the attributes for Leader
	private int leaderId;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private double salary;

	// [Exercise 1.a] Create an empty constructor that does not take any argument.
	/**
	 * Creates an empty leader object.
	 */
	public Leader() {
	}

	// [Exercise 1.b] Create a constructor that accepts firstName and lastName as
	// arguments
	/**
	 * Creates a leader object.
	 *
     * @param firstName the first name of the leader.
	 * @param lastName the last name of the leader.
	 */
	public Leader(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	// [Exercise 1.b] Create a cosntructor that accepts all attributes
	/**
	 * Creates a leader object.
	 *
	 * @param leaderId    the id of the leader.
     * @param firstName    the first name of the leader.
	 * @param lastName    the last name of the leader.
	 * @param phoneNumber the phone number of the leader.
	 * @param salary      the salary of the leader.
	 */
	public Leader(int leaderId, String firstName, String lastName, String phoneNumber, double salary) {
		this.leaderId = leaderId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.salary = salary;
	}

	// [Exercise 1.b] Implement the printFullName() method that prints the full name
	// of the
	// student.
	/**
	 * Prints the full name of the leader to the standard output.
	 */
	public void printFullName() {
		System.out.println("Full name: " + firstName + " " + lastName);
	}

	// [Exercise 1.b] implement the method that returns the salary for certain
	// months
	/**
	 * Calculates the salary for a given number of months.
	 *
	 * @param numberOfMonths the number of months to return the salary for.
	 *
	 * @return the salary for the given number of months.
	 */
	public double calculateSalary(int numberOfMonths) {
		return this.salary * numberOfMonths;
	}

	// [Exercise 1.a] Create getter and setter methods for all attributes
	/**
	 * Gets the id of a leader.
	 *
	 * @return the id of the leader.
	 */
	public int getLeaderId() {
		return leaderId;
	}

	/**
	 * Sets the id of the leader.
	 *
	 * @param leaderId the id to set for the leader.
	 */
	public void setLeaderId(int leaderId) {
		this.leaderId = leaderId;
	}

	/**
	 * Gets the first name of the leader.
	 *
	 * @return the first name of the leader.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name of the leader.
	 *
	 * @param firstName the first name of the leader.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the last name of the leader.
	 *
	 * @return the last name of the leader.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name of the leaer.
	 *
	 * @param lastName the last name of the leader.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the phone number of the leader.
	 *
	 * @return the phone number of the leader.
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * Sets the phone number of the leader.
	 *
	 * @param phoneNumber the phone number of the leader.
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Gets the monthly salary of the leader.
	 *
	 * @return the monthly of the leader.
	 */
	public double getSalary() {
		return salary;
	}

	/**
	 * Sets the monthly salaray of the leader.
	 *
	 * @param salary the monthly salary of the leader.
	 */
	public void setSalary(double salary) {
		this.salary = salary;
	}
}
