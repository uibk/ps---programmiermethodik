package w09_ex03.sheet05;

import org.junit.jupiter.api.Test;
import w09_ex03.sheet05.dependencies.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class ProductManagerTest {

    @Test
    void constructorSizeExceptionTest() {
        assertThrows(IllegalArgumentException.class, () -> new ProductManager(0));
    }

    @Test
    void addProductSizeChangeTest() {
        int assertedSize = 4;
        ProductManager productManager = new ProductManager(4);
        Product sepp = new BasicProduct("Sepp", 2.0);
        Product hugo = new BasicProduct("Hugo", 2.0);
        Product franz = new BasicProduct("Franz", 2.0);
        Product hallo = new BasicProduct("hallo", 2.0);
        try {
            productManager.addProduct(sepp);
            productManager.addProduct(hugo);
            productManager.addProduct(franz);
            productManager.addProduct(hallo);
            assertEquals(assertedSize, productManager.getCurrentSize());
        } catch (StoreFullException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    void addProductTest() {
        ProductManager productManager = new ProductManager(2);
        Product felix = new BasicProduct("Felix", 2);
        try {
            productManager.addProduct(felix);
        } catch (StoreFullException e) {
            System.out.println(e.getMessage());
        }
        assertEquals(productManager.getProductInList(0), felix);

    }

    @Test
    void storeFullTest() {
        ProductManager productManager = new ProductManager(3);
        Product ludwig = new BasicProduct("Ludwig", 2.0);
        Product jack = new BasicProduct("Jack", 2.0);
        Product peter = new BasicProduct("Peter", 2.0);
        Product hallo = new BasicProduct("hallo", 2.0);
        try {
            productManager.addProduct(ludwig);
            productManager.addProduct(jack);
            productManager.addProduct(peter);
            assertThrows(StoreFullException.class, () -> productManager.addProduct(hallo));
        } catch (StoreFullException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    void removeTest() {
        ProductManager productManager = new ProductManager(3);
        Product john = new BasicProduct("John", 2.0);
        Product josh = new BasicProduct("Josh", 2.0);
        Product peter = new BasicProduct("Peter", 2.0);

        try {
            productManager.addProduct(john);
            productManager.addProduct(josh);
            productManager.addProduct(peter);
        } catch (StoreFullException sfe) {
            System.out.println("failed");
        }
        try {
            productManager.removeProduct(john);
        } catch (ProductNotInListException e) {
            System.out.println(e.getMessage());
        }
        assertEquals(productManager.getProductInList(0), josh);
    }
}