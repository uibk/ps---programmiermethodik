package w09_ex03.sheet05.dependencies;

/** This interface represents a product. */
public interface Product {

	/** @return the name of a given product. */
	public String getName();

	/** @return the price of a given product. */
	public double getPrice();
}