package w09_ex03.sheet05.dependencies;

public class BasicProduct implements Product {
	String name;
	double price;

	public BasicProduct(String name, double price) {
		this.name = name;
		this.price = price;
		if (name == null) {
			throw new IllegalArgumentException("no name entered");
		}
		if (price <= 0) {
			throw new IllegalArgumentException("price too low");
		}
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public double getPrice() {
		return this.price;
	}
}