package w09_ex03.sheet05.dependencies;

public class ProductNotInListException extends Exception {
	public ProductNotInListException() {
	}

	public ProductNotInListException(String msg) {
		super(msg);
	}
}