package w09_ex03.sheet05.dependencies;

public class StoreFullException extends Exception {
	public StoreFullException() {
	};

	public StoreFullException(String msg) {
		super(msg);
	}
}