package w09_ex03.sheet05.dependencies;

import java.util.ArrayList;

public class ProductManager {

	private ArrayList<Product> products = new ArrayList<>();
	private int maxSize;
	private int currentSize = 0;

	public ProductManager(int size) throws IllegalArgumentException {
		this.maxSize = size;
		if (size <= 0) {
			throw new IllegalArgumentException("Maximum sizes can not be zero or bellow");
		}
	}

	public void addProduct(Product product) throws StoreFullException {
		if (this.currentSize < maxSize) {
			for (Product loopProduct : this.products) {
				if (loopProduct.getName().equals(product.getName())) {
					this.products.add(product);
					return;
				}
			}

			this.products.add(product);
			this.currentSize++;
			return;

		}
		throw new StoreFullException("Store is already Full");

	}

	public void removeProduct(Product product) throws ProductNotInListException {
		if (this.products.contains(product)) {
			this.products.remove(product);
			if (!this.products.contains(product)) {
				currentSize--;
			}
		} else {
			throw new ProductNotInListException("product is not in list");
		}
	}

	public double totalPrice() {
		double totalPrice = 0;

		for (Product product : this.products) {
			totalPrice += product.getPrice();
		}
		return totalPrice;
	}

	public ArrayList<Product> getProducts() {
		return this.products;
	}

	public int getCurrentSize() {
		return currentSize;
	}

	public Product getProductInList(int number) {
		return this.products.get(number);
	}
}