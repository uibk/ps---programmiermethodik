package w09_ex03.sheet08;

public class Trainee extends Employee {

	private boolean goodPerformance;

	public Trainee(int id, String name, String surname, boolean goodPerformance) {
		super(id, name, surname);
		this.goodPerformance = goodPerformance;
	}

	private boolean calcDidGoodJob(boolean goodJob) {
		return goodJob;
	}

	public int calculateSalary() {
		int bonus = 0;
		if (this.calcDidGoodJob(this.goodPerformance)) {
			bonus += 300;
		}
		return bonus;
	}
}
