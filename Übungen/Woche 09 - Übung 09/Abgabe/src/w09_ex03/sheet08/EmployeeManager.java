package w09_ex03.sheet08;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class EmployeeManager {

    private Map<Integer, Trainee> trainees;
    private Map<Integer, SecurityPersonal> securityPersonal;
    private Map<Integer, SpecializedWorker> specializedWorkers;
    private Map<Integer, DepartmentManager> departmentManagers;

    public EmployeeManager() {
        this.trainees = new HashMap<>();
        this.securityPersonal = new HashMap<>();
        this.specializedWorkers = new HashMap<>();
        this.departmentManagers = new HashMap<>();
    }

    public void addTrainee(Trainee empl) {
        this.trainees.put(empl.getId(), empl);
    }

    public void addSecurityPersonal(SecurityPersonal empl) {
        this.securityPersonal.put(empl.getId(), empl);
    }

    public void addSpecializedWorker(SpecializedWorker empl) {
        this.specializedWorkers.put(empl.getId(), empl);
    }

    public void addDepartmentManager(DepartmentManager empl) {
        this.departmentManagers.put(empl.getId(), empl);
    }

    public void printAll() {
        this.trainees.values().forEach(System.out::println);
        this.securityPersonal.values().forEach(System.out::println);
        this.specializedWorkers.values().forEach(System.out::println);
        this.departmentManagers.values().forEach(System.out::println);
    }

    public Trainee getTraineeById(int id) {
        return this.trainees.get(id);
    }

    public SecurityPersonal getSecurityPersonalById(int id) {
        return this.securityPersonal.get(id);
    }

    public SpecializedWorker getSpecializedWorkerById(int id) {
        return this.specializedWorkers.get(id);
    }

    public DepartmentManager getDepartmentManagerById(int id) {
        return this.departmentManagers.get(id);
    }

    // for testing purposes
    public Collection<Trainee> getAllTrainees() {
        return Collections.unmodifiableCollection(this.trainees.values());
    }

    // for testing purposes
    public Collection<SecurityPersonal> getAllSecurityPersonal() {
        return Collections.unmodifiableCollection(this.securityPersonal.values());
    }

    // for testing purposes
    public Collection<SpecializedWorker> getAllSpecializedWorkers() {
        return Collections.unmodifiableCollection(this.specializedWorkers.values());
    }

    // for testing purposes
    public Collection<DepartmentManager> getAllDepartmentManagers() {
        return Collections.unmodifiableCollection(this.departmentManagers.values());
    }

}
