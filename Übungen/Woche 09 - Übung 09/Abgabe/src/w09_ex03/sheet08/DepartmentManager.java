package w09_ex03.sheet08;

import java.util.Calendar;
import java.util.Date;

public class DepartmentManager extends Employee {

    private int yearOfEngagement;
    private int salary;

    public DepartmentManager(int id, String name, String surname, int yearOfEngagementToSet) {
        super(id, name, surname);
        this.yearOfEngagement = yearOfEngagementToSet;
        this.salary = 1300;
    }

    private int getCurrentYear(Date currentDateParam) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDateParam);
        return cal.get(Calendar.YEAR);
    }

    private int calcEmploymentYears(int yearOfEngagementParam, Date currentDateParam) {
        return this.getCurrentYear(currentDateParam) - yearOfEngagementParam;
    }

    private int calcEmploymentBonus(int yearOfEngagementParam, Date currentDateParam) {
        return this.calcEmploymentYears(yearOfEngagementParam, currentDateParam) * 20;
    }

    public int getBaseSalary() {
        return getSalary() * 2;
    }

    public int calculateSalary() {
        return this.getBaseSalary() + this.calcEmploymentBonus(this.yearOfEngagement, new Date());
    }

    public int getYearOfEngagement() {
        return yearOfEngagement;
    }

    public void setYearOfEngagement(int yearOfEngagement) {
        this.yearOfEngagement = yearOfEngagement;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
