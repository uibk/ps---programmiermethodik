package w09_ex03.sheet08;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SalaryCalculatorTest {

    private EmployeeManager manager;

    @BeforeEach
    public void doInit() {
        manager = createEmployeeManager();
    }

    private EmployeeManager createEmployeeManager() {
        return new EmployeeManager();
    }

    public Trainee createTrainee(int id, String name, String surname, boolean goodJob) {
        return new Trainee(id, name, surname, goodJob);
    }

    public SecurityPersonal createSecurityPersonal(int id, String name, String surname, int workingDays) {
        return new SecurityPersonal(id, name, surname, workingDays);
    }

    public SpecializedWorker createSpecializedWorker(int id, String name, String surname, int bachelorDegress, int masterDegrees) {
        return new SpecializedWorker(id, name, surname, bachelorDegress, masterDegrees);
    }

    public DepartmentManager createDepartmentManager(int id, String name, String surname, int yearOfEngagementToSet) {
        return new DepartmentManager(id, name, surname, yearOfEngagementToSet);
    }

    @Test
    public void testEmptyDepartmentManagers() {
        Assertions.assertTrue(manager.getAllDepartmentManagers().isEmpty());
    }

    @Test
    public void testEmptySecurityPersonal() {
        Assertions.assertTrue(manager.getAllSecurityPersonal().isEmpty());
    }

    @Test
    public void testEmptyTrainees() {
        Assertions.assertTrue(manager.getAllTrainees().isEmpty());
    }

    @Test
    public void testEmptySpecializedWorkers() {
        Assertions.assertTrue(manager.getAllSpecializedWorkers().isEmpty());
    }

    @Test
    public void testNotEmptyTrainees() {
        manager.addTrainee(createTrainee(0, "employeeName", "employeeSurname", false));
        Assertions.assertFalse(manager.getAllTrainees().isEmpty());
    }

    @Test
    public void testTraineeSize() {
        manager.addTrainee(createTrainee(0, "employeeName", "employeeSurname", false));
        Assertions.assertEquals(1, manager.getAllTrainees().size());
    }

    //TODO: more refactor
    @Test
    public void testTraineeNameById() {
        manager.addTrainee(createTrainee(0, "employeeName", "employeeSurname", false));
        Assertions.assertEquals("employeeName", manager.getTraineeById(0).getName());
    }

    @Test
    public void testUnpaidTrainee() {
        manager.addTrainee(createTrainee(0, "employeeName", "employeeSurname", false));
        Assertions.assertEquals(0, manager.getTraineeById(0).calculateSalary());
    }

    @Test
    public void testPaidTrainee() {
        Trainee trainee = createTrainee(0, "employeeName", "employeeSurname", true);
        Assertions.assertEquals(300, trainee.calculateSalary());
    }

    @Test
    public void testSecurityWorkerWithoutBonus() {
        SecurityPersonal personal = createSecurityPersonal(0, "employeeName", "employeeSurname", 12);
        Assertions.assertEquals(1300, personal.calculateSalary());
    }

    @Test
    public void testSecurityWorkerWithBonus() {
        SecurityPersonal personal = createSecurityPersonal(0, "employeeName", "employeeSurname", 26);
        Assertions.assertEquals(1500, personal.calculateSalary());
    }

    @Test
    public void testSpecializedWorkerWithoutTitles() {
        SpecializedWorker specializedWorker = createSpecializedWorker(0, "employeeName", "employeeSurname", 0, 0);
        Assertions.assertEquals(1300, specializedWorker.calculateSalary());
    }

    @Test
    public void testSpecializedWorkerWithSingleTitles() {
        SpecializedWorker specializedWorker = createSpecializedWorker(0, "employeeName", "employeeSurname", 1, 1);
        Assertions.assertEquals(2500, specializedWorker.calculateSalary());
    }

    @Test
    public void testSpecializedWorkerWithMultipleTitles() {
        SpecializedWorker specializedWorker = createSpecializedWorker(0, "employeeName", "employeeSurname", 3, 3);
        Assertions.assertEquals(3100, specializedWorker.calculateSalary());
    }

    @Test
    public void testDepartmentManagerWithoutBonus() {
        DepartmentManager departmentManager = createDepartmentManager(0, "employeeName", "employeeSurname", 2019);
        Assertions.assertEquals(2600, departmentManager.calculateSalary());
    }

    @Test
    public void testDepartmentManagerWithBonus() {
        DepartmentManager departmentManager = createDepartmentManager(0, "employeeName", "employeeSurname", 2009);
        Assertions.assertEquals(2800, departmentManager.calculateSalary());
    }
}
