package w09_ex03.sheet08;

public class SecurityPersonal extends Employee {

    private int workingDays;
    private int salary;

    public SecurityPersonal(int id, String name, String surname, int workingDaysToSet) {
        super(id, name, surname);
        this.workingDays = workingDaysToSet;
        this.salary = 1300;
    }

    public int calculateSalary() {
        if ((this.workingDays >= 25)) {
            salary += 200;
        }
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
