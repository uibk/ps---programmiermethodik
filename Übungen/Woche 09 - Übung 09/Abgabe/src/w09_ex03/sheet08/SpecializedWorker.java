package w09_ex03.sheet08;

public class SpecializedWorker extends Employee {

	private int nrBachelorDegrees;
	private int nrMasterDegrees;
	private int bonus;

	public SpecializedWorker(int id, String name, String surname, int bachelorDegrees, int masterDegrees) {
		super(id, name, surname);
		this.nrBachelorDegrees = bachelorDegrees;
		this.nrMasterDegrees = masterDegrees;
		this.bonus = 0;
	}

	public int calculateSalary() {
		return 1300 + calculateBachelorBonus() + calculateMasterBonus(bonus);
	}

	private int calculateBachelorBonus() {
		int bonus = 0;
		if (nrBachelorDegrees == 1) {
			bonus = 400;
		} else if (nrBachelorDegrees > 1) {
			bonus = nrBachelorDegrees * 200;
		}
		return bonus;
	}

	private int calculateMasterBonus(int bonus) {
		if (nrMasterDegrees == 1) {
			bonus += 800;
		} else if (nrMasterDegrees > 1) {
			bonus += nrMasterDegrees * 400;
		}
		return bonus;
	}
}
