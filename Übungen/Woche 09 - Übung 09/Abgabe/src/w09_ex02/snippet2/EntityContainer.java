package w09_ex02.snippet2;

import java.util.ArrayList;
import java.util.List;

/**
 * A class to collect Entities.
 *
 */
public class EntityContainer {

    public List<Entity> container;

    public EntityContainer() {
        container = new ArrayList<>();
    }
}
