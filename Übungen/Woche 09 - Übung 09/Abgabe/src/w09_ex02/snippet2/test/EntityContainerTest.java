package w09_ex02.snippet2.test;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import w09_ex02.snippet2.Entity;
import w09_ex02.snippet2.EntityContainer;

import java.util.ArrayList;


public class EntityContainerTest {

    private EntityContainer entityContainer;

    @BeforeEach
    public void doInit() {
        this.entityContainer = new EntityContainer();
    }

    @Test
    public void testContains() {
        Entity entity = new Entity("name");
        this.entityContainer.container.add(entity);

        Assertions.assertTrue(this.entityContainer.container.contains(entity));
        Assertions.assertTrue(this.entityContainer.container.contains(new Entity("name")));
    }
    /*
    The problem here was that no equals or hashcode method was provided.
    Therefore, when you pass in a new created instance of the class Entity it differs from the previous created object and the test fails
     */
}
