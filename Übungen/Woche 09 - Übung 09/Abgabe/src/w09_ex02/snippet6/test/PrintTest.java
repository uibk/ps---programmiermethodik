package w09_ex02.snippet6.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import w09_ex02.snippet6.D;

public class PrintTest {
	
	@Test
	public void doTest() {
		Assertions.assertEquals("ABCD", new D().doPrint());
	}
/*
	The problem was that D was a sub class from C -> B and
	therefore A but D didn't invoke the super method of it's super class and wasn't able to get their values from this call.
 */
}
