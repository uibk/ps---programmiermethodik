package w09_ex02.snippet6;

/**
 * Another meaningless class.
 */
public class C extends B {

    @Override
    public String doPrint() {
        return super.doPrint() + "C";
    }
}
