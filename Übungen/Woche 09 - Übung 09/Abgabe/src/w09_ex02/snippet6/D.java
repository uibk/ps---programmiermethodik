package w09_ex02.snippet6;

/**
 * Another meaningless class.
 *
 */
public class D extends C {

	@Override
	public String doPrint() {
		return super.doPrint()+"D";
	}

}
