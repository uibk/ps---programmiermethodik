package w09_ex02.snippet6;

/**
 * A meaningless class.
 *
 */
public class A {

	/**
	 * Returns a letter as string.
	 * 
	 * @return
	 */
	public String doPrint() {
		return "A";
	}

}
