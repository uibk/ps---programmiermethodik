package w09_ex02.snippet6;

/**
 * Another meaningless class.
 */
public class B extends A {

    @Override
    public String doPrint() {
        return super.doPrint() + "B";
    }

}
