package w09_ex02.snippet3.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import w09_ex02.snippet3.Person;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class PersonTest {

	@Test
	public void testPerson() {
		List<Person> personList = new ArrayList<>();
		Set<Person> personSet = new HashSet<>();

		personList.add(new Person("name", "surname"));
		personSet.add(new Person("name", "surname"));

		Assertions.assertTrue(personList.contains(new Person("name", "surname")));
		Assertions.assertTrue(personSet.contains(new Person("name", "surname")));
	}

}
