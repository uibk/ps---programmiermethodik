package w09_ex02.snippet1.test;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import w09_ex02.snippet1.StringContainer;

public class StringContainerTest {

	private StringContainer stringContainer;

	@BeforeEach
	public void doInit() {
		this.stringContainer = new StringContainer();
	}

	@Test
	public void testContains() {
		this.stringContainer.addString("car");
		this.stringContainer.addString("bird");

		Assertions.assertFalse(this.stringContainer.containsString("animal"));
	}
/*
You simply need to init a proper string container
 */
}
