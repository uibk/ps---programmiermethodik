package w09_ex02.snippet1;

import java.util.ArrayList;
import java.util.List;

/**
 * This class manages strings.
 */
public class StringContainer {

    private List<String> stringContainer;

    public StringContainer() {
        this.stringContainer = new ArrayList<>();
    }

    /**
     * Adds a string to a string container
     *
     * @param string
     */
    public void addString(String string) {
        this.stringContainer.add(string);
    }

    /**
     * Checks if a string is contained within the string collection.
     *
     * @param string
     * @return
     */
    public boolean containsString(String string) {
        for (String stringEntry : this.stringContainer) {
            if (stringEntry.equals(string)) {
                return true;
            }
        }
        return false;
    }
}
