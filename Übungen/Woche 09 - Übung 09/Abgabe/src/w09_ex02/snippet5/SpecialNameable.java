package w09_ex02.snippet5;

/**
 * Represents a subclass of a nameable entity.
 */
public class SpecialNameable extends Nameable {

	public SpecialNameable(String name, int value) {
        super(name, value);
	}

	/**
	 * Gets an entities name.
	 * 
	 * @return
	 */
	public String getName() {
        return super.getName();
	}
	
	/**
	 * Gets an entities value.
	 * 
	 * @return
	 */
    public int getValue() {
        return super.getValue();
	}
	
}
