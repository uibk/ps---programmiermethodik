package w09_ex02.snippet5;

/**
 * Represents an entity which has a name.
 */
public class Nameable {

    private String name;
    private int value;

    public Nameable(String name) {
        this.name = name;
    }

    public Nameable(String name, int value) {
        this.name = name;
        this.value = value;
    }

	/**
	 * Gets an entities name.
	 * 
	 * @return
	 */
	public String getName() {
        return name;
	}

	/**
	 * Gets an entities value.
	 * 
	 * @return
	 */
	public int getValue() {
        return value;
	}
	
}
