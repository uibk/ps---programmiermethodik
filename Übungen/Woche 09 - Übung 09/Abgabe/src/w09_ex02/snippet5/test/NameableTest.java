package w09_ex02.snippet5.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import w09_ex02.snippet5.Nameable;
import w09_ex02.snippet5.SpecialNameable;

public class NameableTest {

    @Test
    public void testNameable() {
        Nameable nameable = new Nameable("name");
        Nameable otherNameable = new SpecialNameable("name", 123);

        Assertions.assertEquals(nameable.getName(), otherNameable.getName());
        Assertions.assertNotEquals(nameable.getValue(), otherNameable.getValue());
    }
	/*
	I added the value and corresponding getters and setters to the super class Nameable as we invoke the get value with the super type Nameable and not SpecialNameable
	 */
}
