package w09_ex02.snippet4.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import w09_ex02.snippet4.ShiftList;


public class ShiftListTest {

    private ShiftList shiftList;

    @BeforeEach
    public void doInit() {
        shiftList = new ShiftList();
    }

	@BeforeEach
	public void addEntries() {
		this.shiftList.addLeft("a");
		this.shiftList.addLeft("b");
		this.shiftList.addLeft("c");

		this.shiftList.addRight("d");
		this.shiftList.addRight("e");
		this.shiftList.addRight("f");
	}

	@Test
	public void testShift() {
		this.shiftList.shiftLeft();
		Assertions.assertEquals(6, this.shiftList.getLeft().size());
		Assertions.assertEquals(0, this.shiftList.getRight().size());
	}

	@Test
	public void testInit() {
		this.shiftList.clearLeftList(this.shiftList.getLeft());
		Assertions.assertEquals(0, this.shiftList.getLeft().size());
		Assertions.assertEquals(6, this.shiftList.getRight().size());	
	}
	
}
