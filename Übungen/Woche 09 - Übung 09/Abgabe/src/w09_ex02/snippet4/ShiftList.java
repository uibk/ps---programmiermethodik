package w09_ex02.snippet4;

import java.util.ArrayList;
import java.util.List;

/**
 * Realizes a simple implementation of a shift list: Items within one list can
 * be selected and shifted to the other list.
 */
public class ShiftList {

    private List<String> left;
    private List<String> right;

    public ShiftList() {
        left = new ArrayList<>();
        right = new ArrayList<>();
    }

    /**
     * Shifts items from src to a dest-list.
     *
     * @param src
     * @param dest
     */
    private void shift(List<String> src, List<String> dest) {
        dest.addAll(src);
        src.removeAll(dest);
    }

    /**
     * Convienience method: shifts items from right to left.
     */
    public void shiftLeft() {
        this.shift(this.right, this.left);
    }

    /**
     * Convienience method: shifts items from left to right.
     */
    public void shiftRight() {
        this.shift(this.left, this.right);
    }

    /**
     * Clears the left list by moving all its content to the other list.
     *
     * @param elementsToClear
     */
    public void clearLeftList(List<String> elementsToClear) {
        List<String> tmp = new ArrayList<>(elementsToClear);
        this.left.removeAll(tmp);
        this.right.addAll(tmp);
    }

    /**
     * Adds a string to the left list.
     *
     * @param string
     */
    public void addLeft(String string) {
        this.left.add(string);
    }

    /**
     * Adds a string to the right list.
     *
     * @param string
     */
    public void addRight(String string) {
        this.right.add(string);
    }

    /**
     * @return the left
     */
    public List<String> getLeft() {
        return left;
    }

    /**
     * @param left the left to set
     */
    public void setLeft(List<String> left) {
        this.left = left;
    }

    /**
     * @return the right
     */
    public List<String> getRight() {
        return right;
    }

    /**
     * @param right the right to set
     */
    public void setRight(List<String> right) {
        this.right = right;
    }
}
