package w09_ex02.snippet7;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A simple implementation to read the content out of a file.
 */
public class SimpleFileReader {

    private final String BASE_PATH = "/Übungen/Woche 09 - Übung 09/Abgabe/src/w09_ex02/snippet7/data/";

    /**
     * Builds the absolute file-path.
     *
     * @param fileName
     * @return
     */
    private String buildPath(String fileName) {
        return new File("").getAbsolutePath() + BASE_PATH + fileName;
    }

    /**
     * Reads all lines of a file and returns them inside a String-list.
     *
     * @param fileName
     * @return
     */
    public List<String> readFromFile(String fileName) {
        try (
                BufferedReader reader = new BufferedReader(new FileReader(new File(buildPath(fileName))))
        ) {
            String line;
            List<String> content = new ArrayList<>();
            while ((line = reader.readLine()) != null) {
                content.add(line);
            }
            return content;
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return Collections.emptyList();
    }

}
