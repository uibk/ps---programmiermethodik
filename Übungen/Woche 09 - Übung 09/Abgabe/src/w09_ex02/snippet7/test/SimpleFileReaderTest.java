package w09_ex02.snippet7.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import w09_ex02.snippet7.SimpleFileReader;


public class SimpleFileReaderTest {

	private SimpleFileReader fileReader;

	@BeforeEach
	public void doInit() {
		fileReader = new SimpleFileReader();
	}

	@Test
	public void testFileRead() {
		Assertions.assertFalse(fileReader.readFromFile("Test.txt").isEmpty());
		Assertions.assertEquals(1, fileReader.readFromFile("Test.txt").size());

		String content = fileReader.readFromFile("Test.txt").get(0);
		String[] splitted = content.split(";");
		Assertions.assertEquals(5, splitted.length);
	}
/*
	The given Test.txt and the relative semicolons were not encoded correctly and the system wouldn't recognize this path.
	Apart from that, the buffered reader wasn't instantiated correctly and closed, respectively.
 */
}
