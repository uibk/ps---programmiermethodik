package w09_ex01;

import w09_ex01.util.MyException;

public interface Provider extends Event {

	public boolean hasNext() throws MyException;

	public char nextContent() throws MyException;

}
