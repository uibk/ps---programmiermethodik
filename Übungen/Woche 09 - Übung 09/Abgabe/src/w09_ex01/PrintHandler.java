package w09_ex01;


import w09_ex01.util.MyException;

public interface PrintHandler extends Event {

	public abstract void printOutput(char output) throws MyException;

}
