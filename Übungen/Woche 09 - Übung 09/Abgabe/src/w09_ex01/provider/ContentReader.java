package w09_ex01.provider;

import w09_ex01.Provider;
import w09_ex01.util.MyException;
import w09_ex01.util.Utils;

import java.io.*;


public class ContentReader implements Provider {

	private String name;
	private Reader stream;

	public ContentReader(String fileName) {
		super();
		this.name = fileName;
	}

	private boolean isInitialized() {
		return this.stream != null;
	}

	private void initBufferedReader() throws FileNotFoundException {
        this.stream = new BufferedReader(new FileReader(Utils.getBaseFilePath(name)));
	}

	private void initIfNecessary() throws FileNotFoundException {
		if (!this.isInitialized()) {
			this.initBufferedReader();
		}
	}

	@Override
	public void onInit() throws MyException {
		try {
			this.initIfNecessary();
		} catch (FileNotFoundException e) {
			throw new MyException(e);
		}
	}

	@Override
	public boolean hasNext() throws MyException {
		try {
			return this.stream.ready();
		} catch (IOException e) {
			throw new MyException(e);
		}
	}

	@Override
	public char nextContent() throws MyException {
		try {
			return (char) this.stream.read();
		} catch (IOException e) {
			throw new MyException(e);
		}
	}

	@Override
	public void onExit() {
		try {
			this.stream.close();
		} catch (IOException e) {
			// not closable? houston, we have a problem :D
			e.printStackTrace();
		}
	}
}
