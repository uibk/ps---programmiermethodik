package w09_ex01;


import w09_ex01.util.MyException;

public interface Event {

	public default void onInit() throws MyException {
		/* override if required */
	}

	public default void onExit() {
		/* override at subclass if required */
	}
	
}
