package w09_ex01;

public interface Module {
	
	char getEncryptedInput(char input, String keyName);

	char getDecryptedInput(char input, String keyName);

}
