package w09_ex01.util;

import w09_ex01.OtherProvider;

import java.util.Date;


public class TimeStampProvider implements OtherProvider<Character> {

    private long generateTimeStamp() {
        return new Date().getTime();
    }

    private void saveTimeStamp(String name, long value) {
        Utils.writeToValueFile(name, value + "");
    }

    private char getEncryptedChar(long value) {
        return (char) (value % Character.MAX_VALUE);
    }

    @Override
    public void provideCurrentTimestamp(String keyName) {
        long value = this.generateTimeStamp();
        this.saveTimeStamp(keyName, value);
    }

    @Override
    public Character loadEncryptedChar(String keyName) {
        return this.getEncryptedChar(Utils.getTimeStampAsLong(keyName));
    }

}
