package w09_ex01.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

public class Utils {

    public static String BASE_PATH = "/Übungen/Woche 09 - Übung 09/Abgabe/src/w09_ex01/w09_ex02.snippet7.data/";
    public static String VALUE_PATH = BASE_PATH + "val/";
    public static String OUTPUT_PATH = BASE_PATH + "output/";

	private Utils() {
	}

    private static String getFilePath(String basePath, String fileName) {
		return new File("").getAbsolutePath() + basePath + fileName;
	}

    public static String getBaseFilePath(String fileName) {
        return getFilePath(BASE_PATH, fileName);
    }

    public static String getValueFilePath(String fileName) {
        return getFilePath(VALUE_PATH, fileName);
    }

    public static String getOutputFilePath(String fileName) {
        return getFilePath(OUTPUT_PATH, fileName);
	}

    public static void writeToFile(String path, List<String> content) throws IOException {
		Files.write(new File(path).toPath(), content);
	}

    public static void writeToValueFile(String keyName, String key) {
		try {
            writeToFile(getValueFilePath(keyName), Arrays.asList(key));
		} catch (IOException e) {
			System.err.println("Could not write");
		}
	}

    public static List<String> readTimeStampFromFile(String path) throws IOException {
		return Files.readAllLines(new File(path).toPath());
	}

    public static Long getTimeStampAsLong(String keyName) {
		try {
            return Long.parseLong(readTimeStampFromFile(getValueFilePath(keyName)).get(0));
		} catch (NumberFormatException | IOException e) {
			System.err.println("Could not read.");
			return 4L;
        }
    }

    public static boolean fileExists(String path) {
		return Files.exists(new File(path).toPath());
	}

	public static boolean itExists2(String keyName) {
        return fileExists(getValueFilePath(keyName));
	}

}
