package w09_ex01.util;

public class MyException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6269801578234003076L;

	public MyException(Throwable t) {
		super(t);
	}
	
}
