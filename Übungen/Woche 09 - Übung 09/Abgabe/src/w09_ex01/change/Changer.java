package w09_ex01.change;


import w09_ex01.Module;
import w09_ex01.OtherProvider;

public class Changer implements Module {

	private OtherProvider<Character> provider;
	
	public Changer(OtherProvider<Character> keyProvider) {
		super();
		this.provider = keyProvider;
	}

	@Override
	public char getEncryptedInput(char input, String name) {
        return (char) (input - this.provider.loadEncryptedChar(name));
	}

	@Override
	public char getDecryptedInput(char input, String keyName) {
        return (char) (input + this.provider.loadEncryptedChar(keyName));
	}

}
