package w09_ex01;

public interface OtherProvider<T> {

    void provideCurrentTimestamp(String keyName);

    T loadEncryptedChar(String keyName);

}