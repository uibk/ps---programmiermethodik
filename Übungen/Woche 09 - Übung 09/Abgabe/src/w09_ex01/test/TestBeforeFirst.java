package w09_ex01.test;


import w09_ex01.Module;
import w09_ex01.*;
import w09_ex01.change.Changer;
import w09_ex01.print.WriteHandler;
import w09_ex01.print.OutputHandler;
import w09_ex01.provider.ContentReader;
import w09_ex01.util.TimeStampProvider;

public class TestBeforeFirst {

	private static final String DEFAULT_NAME = "default";

	public static void main(String[] args) {
        OtherProvider<Character> provider = new TimeStampProvider();
        provider.provideCurrentTimestamp(DEFAULT_NAME);

		Module module = new Changer(provider);
		MainModule manager = new MainModule(module);

		Provider contentReader = new ContentReader("1.txt");
		PrintHandler outputHandler = new OutputHandler();
		PrintHandler writeHandler = new WriteHandler("1.txt");

		System.out.print("Output: ");
        manager.encrypt(contentReader, DEFAULT_NAME, outputHandler, writeHandler);

    }
/*
First, this program writes the current timestamp in a file under the package /val/
out of this timestamp and given characters in 1.txt under /w09_ex02.snippet7.data/ directory it encrypts these characters in chinese letters and prints on standard output and stores it under /output/1.txt
 */
}
