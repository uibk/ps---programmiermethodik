package w09_ex01.test;

import w09_ex01.Module;
import w09_ex01.*;
import w09_ex01.change.Changer;
import w09_ex01.print.OutputHandler;
import w09_ex01.provider.ContentReader;
import w09_ex01.util.TimeStampProvider;

public class TestFirst {

	private static final String DEFAULT_NAME = "default";

	public static void main(String[] args) {
        OtherProvider<Character> provider = new TimeStampProvider();

		Module module = new Changer(provider);
		MainModule manager = new MainModule(module);

		PrintHandler output = new OutputHandler();
		Provider fancyProvider = new ContentReader("output/1.txt");

		System.out.print("Output: ");
        manager.decrypt(fancyProvider, DEFAULT_NAME, output);

    }
/*
this program is the reversed version of the TestBeforeFirst file.
It takes the encrypted characters stored under /output/1.txt and converts it into the original text
which is similar to /data/1.txt and prints it to the standard output and doesn't save it anywhere
 */
}
