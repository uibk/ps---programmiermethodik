package w09_ex01;

@FunctionalInterface
public interface RandomHandler {

	public char processInput(char input, String name);

}
