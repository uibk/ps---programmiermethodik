package w09_ex01;


import w09_ex01.util.MyException;

public class MainModule {

    private Module module;

    public MainModule(Module cryptoModule) {
        super();
        this.module = cryptoModule;
    }

    private void invokeOnInit(Event... allHandlers) throws MyException {
        for (Event handler : allHandlers) {
            handler.onInit();
        }
    }

    private void invokeOnExit(Event... allHandlers) {
        for (Event handler : allHandlers) {
            handler.onExit();
        }
    }

    private void execSomething(Provider provider, RandomHandler handler, String name, PrintHandler... otherHandler) {
        try {
            this.invokeOnInit(provider);
            this.invokeOnInit(otherHandler);

            while (provider.hasNext()) {
                char processedChar = handler.processInput(provider.nextContent(), name);
                for (PrintHandler actualHandler : otherHandler) {
                    actualHandler.printOutput(processedChar);
                }
            }
        } catch (MyException e) {
            System.err.println("An error occurred: " + e.getMessage());
        } finally {
            this.invokeOnExit(provider);
            this.invokeOnExit(otherHandler);
        }
    }

    public void encrypt(Provider provider, String name, PrintHandler... handler) {
        this.execSomething(provider, this.module::getDecryptedInput, name, handler);
    }

    public void decrypt(Provider provider, String name, PrintHandler... outputHandler) {
        this.execSomething(provider, this.module::getEncryptedInput, name, outputHandler);
    }

}
