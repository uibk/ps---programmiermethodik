package w09_ex01.print;


import w09_ex01.PrintHandler;
import w09_ex01.Provider;

public class Combined implements Provider, PrintHandler {

	private String value = "";
	private int i = -1;

	@Override
	public boolean hasNext() {
		return i < this.value.length() - 1;
	}

	@Override
	public char nextContent() {
		return this.value.charAt(++this.i);
	}

	@Override
	public void printOutput(char output) {
		this.value += output;
	}

}
