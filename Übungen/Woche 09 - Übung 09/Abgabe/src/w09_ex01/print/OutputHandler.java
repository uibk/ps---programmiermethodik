package w09_ex01.print;


import w09_ex01.PrintHandler;

public class OutputHandler implements PrintHandler {

	@Override
	public void printOutput(char output) {
		System.out.print(output);
	}

}
