package w09_ex01.print;

import w09_ex01.PrintHandler;
import w09_ex01.util.MyException;
import w09_ex01.util.Utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;


public class WriteHandler implements PrintHandler {

	private String name;
	private Writer stream;

	public WriteHandler(String name) {
		super();
		this.name = name;
	}

	private boolean isInitialized() {
		return this.stream != null;
	}

	private void initBufferedWriter() throws IOException {
        this.stream = new BufferedWriter(new FileWriter(Utils.getOutputFilePath(name)));
	}

	private void initIfNecessary() throws IOException {
		if (!this.isInitialized()) {
			this.initBufferedWriter();
		}
	}

	@Override
	public void printOutput(char output) throws MyException {
		try {
			this.stream.write(output);
		} catch (IOException e) {
			throw new MyException(e);
		}
	}

	@Override
	public void onInit() throws MyException {
		try {
			this.initIfNecessary();
		} catch (IOException e) {
			throw new MyException(e);
		}
	}

	@Override
	public void onExit() {
		try {
			this.stream.close();
		} catch (IOException e) {
			// not closable?
			e.printStackTrace();
		}
	}

}
