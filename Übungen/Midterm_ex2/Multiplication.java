public class Multiplication extends Expression {

    private Expression leftOperand;
    private Expression rightOperand;

    public Multiplication(Expression leftOperand, Expression rightOperand) {
        this.leftOperand = leftOperand;
        this.rightOperand = rightOperand;
    }

    @Override
    public double getValue() {
        return leftOperand.getValue() * rightOperand.getValue();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("(").append(leftOperand.toString()).append(" ").append("* ").append(rightOperand.toString()).append(")");
        return sb.toString();
    }
}
