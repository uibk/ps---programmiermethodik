public abstract class Expression {

    public abstract double getValue();

    public abstract String toString();
}
