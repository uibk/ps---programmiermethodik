public class Literal extends Expression {
    private double literal;

    public Literal(double literal) {
        this.literal = literal;
    }

    public double getLiteral() {
        return literal;
    }

    public void setLiteral(double literal) {
        this.literal = literal;
    }

    @Override
    public double getValue() {
        return literal;
    }

    @Override
    public String toString() {
        return String.valueOf(literal);
    }
}
