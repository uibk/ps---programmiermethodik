package w03_ex02;

public class TurnableTest {
    public static void main(String[] args) {
        Turnable[] turnables = {new Leaf(), new Page(), new Pancake(), new Card(), new Bottle()};

        for (Turnable turnable :
                turnables) {
            turnable.turn();
        }
    }
}
