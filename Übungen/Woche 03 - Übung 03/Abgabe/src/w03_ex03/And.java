package w03_ex03;

public class And implements FormValidator {

    private FormValidator leftOperand, rightOperand;

    And(FormValidator leftOperand, FormValidator rightOperand) {
        this.leftOperand = leftOperand;
        this.rightOperand = rightOperand;
    }

    @Override
    public boolean validate(Form toValidate) {
        return leftOperand.validate(toValidate) && rightOperand.validate(toValidate);
    }
}
