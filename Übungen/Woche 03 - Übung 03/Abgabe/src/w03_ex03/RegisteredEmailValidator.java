package w03_ex03;

import java.util.List;

public class RegisteredEmailValidator implements FormValidator {

    private List<Form> registeredForms;

    public RegisteredEmailValidator(List<Form> registeredForms) {
        this.registeredForms = registeredForms;
    }

    @Override
    public boolean validate(Form toValidate) {
        for (Form form :
                registeredForms) {
            if (form.getEmail().equals(toValidate.getEmail())) {
                return true;
            }
        }
        return false;
    }
}
