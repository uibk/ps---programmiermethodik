package w03_ex03;

public interface FormValidator {

    /**
     * Validates if the given form is valid
     *
     * @param toValidate
     * @return true if the form is correctly fill in
     */
    boolean validate(Form toValidate);

}
