package w03_ex03;

public class EmailValidator implements FormValidator {

    private final char[] SPECIAL_CHARS = "'@', '!', '#', '$', '%', '^', '&', '*', '(', ')', '-', '/', '~', '[', ']".toCharArray();

    @Override
    public boolean validate(Form toValidate) {
        String email = toValidate.getEmail();
        String string = email.replaceFirst("@", "");
        return stringContainsItem(string);
    }

    private boolean stringContainsItem(String input) {
        for (char specialChar : this.SPECIAL_CHARS) {
            if (input.contains(String.valueOf(specialChar))) {
                return false;
            }
        }
        return true;
    }
}
