package w03_ex03;

public class MaximumLengthValidator implements FormValidator {

    private final int MAX_FULLNAME_LENGTH = 30;
    private final int MAX_EMAIL_LENGTH = 20;
    private final int MAX_COUNTRY_LENGTH = 10;

    @Override
    public boolean validate(Form toValidate) {
        return (toValidate.getFirstName().length() + toValidate.getLastName().length() <= MAX_FULLNAME_LENGTH)
                && (toValidate.getEmail().length() <= MAX_EMAIL_LENGTH)
                && (toValidate.getCountry().length() <= MAX_COUNTRY_LENGTH);
    }
}
