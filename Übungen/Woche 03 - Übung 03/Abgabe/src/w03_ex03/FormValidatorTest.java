package w03_ex03;

import java.util.ArrayList;
import java.util.List;

public class FormValidatorTest {
    /**
     * Main method
     *
     * @param args
     */
    public static void main(String[] args) {

        //General list of forms
        List<Form> listOfForms = new ArrayList<>();

        //TODO: Create a list of validated forms
        List<Form> listOfValidatedForms = new ArrayList<>();


        //Adding new forms to the general list of forms (listOfForms)
        listOfForms.add(new Form("Jaim", "Kants", "Jaime@uibkg@bk.at", "Austria"));
        listOfForms.add(new Form("Carla", "Rodriguez", "Carla.Heiko@uiggg@dah.de", "Germany"));
        listOfForms.add(new Form("Mathias", "Chan", "Mathias@hotmail.es", "Spain"));
        listOfForms.add(new Form("Ling", "Gencai", "Ling.Ling@163dot.com", "China"));
        listOfForms.add(new Form("Cintia", "Gerogala", "Georgala@informatik.de", "Germany"));
        listOfForms.add(new Form("Kent", "Prejt", "Kent.Prejt@163dot.com", "China"));
        listOfForms.add(new Form("Guido", "Cheqma", "guido@informatik.es", "Spain"));
        listOfForms.add(new Form("Catia", "Rodriguez", "heiko@uiggg@dah.de", "Germany"));

        //TODO: Create validators
        FormValidator emailValidator = new EmailValidator();
        FormValidator lengthValidator = new MaximumLengthValidator();
        FormValidator registeredEmailValidator = new RegisteredEmailValidator(listOfValidatedForms);

        //TODO: Create finalValidator which combines validators (use And class)
        FormValidator finalValidator = new And(emailValidator, lengthValidator);

        //TODO: Validate each form from the listOfForms and add correct ones to the listOfValidatedForms
        for (Form form :
                listOfForms) {
            if (finalValidator.validate(form)) {
                if (!registeredEmailValidator.validate(form)) {
                    listOfValidatedForms.add(form);
                }
            }
        }

        //TODO: Print the listOfValidatedForms
        printValidatedForms(listOfValidatedForms);
    }

    //TODO: Implement the print method and print the listOfValidatedForms
    public static void printValidatedForms(List<Form> listOfValidatedForms) {
        System.out.printf("%-20s %-25s %-15s\n", "NAME", "EMAIL", "COUNTRY");
        for (Form f :
                listOfValidatedForms) {
            System.out.printf("%-20s %-25s %-15s\n", f.getFirstName() + " " + f.getLastName(), f.getEmail(), f.getCountry());
        }
    }
}
