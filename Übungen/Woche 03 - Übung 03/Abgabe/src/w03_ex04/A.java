package w03_ex04;

public class A {

    /**
     * prints "ex04.A prints"
     */
    public void print() {
        System.out.println("A prints");
    }

    /**
     * prints "A 1"
     *
     * @param a
     */
    public void print(A a) {
        System.out.println("A 1");
    }
}
