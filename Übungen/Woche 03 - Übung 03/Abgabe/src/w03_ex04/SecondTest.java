package w03_ex04;

public class SecondTest {

    /**
     * main method
     *
     * @param args
     */
    public static void main(String[] args) {
        B object1 = new C(); // Assigning C object from Type B => valid, because C is subclass from B => reverse not possible!!
        object1.print(new A()); // Invoking print method on Type C => C 1 is printed out => A type is required and C is a subtype of B => so equivalent to B type =>  valid => C1
        ((C) object1).print(new B()); // Casting object1 to type C and invoke print method on this object => B type is required and since C is subtype of B => valid => C2

        separateOutput(2);

        A object2 = new B(); // Assigning object2 from Type A to type B => valid, because B is subtype from A => dynamic data type => is going to be B at run-time
        ((B) object2).print(new A()); // Casting object2 to type B and invoke print method on this object => B type is required and since C is subtype of B => valid => B1
        object2.print(new C()); // Object2 which is from Type mother-type A and sub-type B prints with a new instantiated C => C subtype from B => subtype from A => B1

        separateOutput(3);

        A object3 = new C(); // C is sub-type from B which is sub-type from A => valid => dynamic data type => is going to be C at run-time
        object3.print(new C()); // Print method from C type => C1 is printed out because real type is C and parameter is basically C but A as mother-type => A is strong mother-type => C is taken as A type
        object3.print(new A()); // Same as above, A is passed into parameter => C1
        object3.print(new B()); // B is sub-type from A as well => C1
        ((C) object3).print(new B()); // When object3 is casted to C then C2 is printed => because C is now generally given type and not A anymore => C2 because B is passed into parameter
        ((C) object3).print(new C()); // Object3 again casted to C and C passed as parameter => C3

        separateOutput(4);

        C object4 = new C(); // Normal instantiation of C type object => static data type
        object4.print(new C()); // C3 => print method of C type and C as parameter
        object4.print(new A()); // C1 => print method of C type but A as parameter
        object4.print(new B()); // C2 => print method of C type but B as parameter

        separateOutput(5);

        B object5 = new B(); // Normal instantiation of C type object => static data type
        object5.print(new C()); // B2 => C is sub-type from B and therefore as valid as B
        object5.print(new B()); // same B2 => B is static type here and B2 will be printed out
        object5.print(new A()); // B1 => because A is given as static type

        separateOutput(6);

        Object object6 = new C(); // Instantiation of object6 as Object Type => valid, because object is mother-class of all classes
        ((C) object6).print(new B()); // Casting to type C, print method of C is invoked and then B is passed into parameter => C2
        ((A) object6).print(new A()); // Casting to type A, print method of C is invoked due to dynamic typing => casting doesn't make any difference here and A is passed as parameter => C1
        ((B) object6).print(new B()); // casting to type B as dynamic Type doesn't make any difference as well => B is passed => C2
        // all of the methods above use dynamic type => C is type at run-time

        separateOutput(7);

        Object object7 = new A();
        ((A) object7).print(new C()); // A is dynamic type => print method of A is invoked and C is passed => C is sub-type of B and therefore A => A1

        separateOutput(8);

        Object object8 = new B();
        ((B) object8).print(new A()); // type at run-time is B and A is passed => B1
        ((A) object8).print(new A()); // same as above, type at run-time is B , doesn't matter of casting anyway, A is passed => B1
    }

    /**
     * adds a separator line
     *
     * @param part
     */
    private static void separateOutput(int part) {
        System.out.println("\n--- " + part + " ---\n");
    }
}
