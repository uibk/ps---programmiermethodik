package w03_ex04;

public class C extends B {

    /**
     * prints "C prints"
     */
    public void print() {
        System.out.println("C prints");
    }

    /**
     * prints "C 1"
     *
     * @param a
     */
    public void print(A a) {
        System.out.println("C 1");
    }

    /**
     * prints "C 2"
     *
     * @param b
     */
    public void print(B b) {
        System.out.println("C 2");
    }

    /**
     * prints "C 3"
     *
     * @param c
     */
    public void print(C c) {
        System.out.println("C 3");
    }
}
