package w03_ex04;

public class FirstTest {

    /**
     * main method
     *
     * @param args
     */
    public static void main(String[] args) {

        A objectA = new A();    // Creating an object called "objectA" of "A" class
        B objectB = new B();    // Creating an object called "objectB" of "B" class
        C objectC = new C();    // Creating an object called "objectC" of "C" class

        A anObject;                // Creating an variable called "anObject" of Type "A"
        anObject = objectA;        // Assign anObject to objectA (which is from Type A) => valid
        anObject.print();          // Invoke print() method on A type => A prints
        anObject = objectB;        // Assign anObject to objectB => Assign Type A to Type B => valid because B extends A (subclass) => reverse approach not possible!!
        anObject.print();          // Invoke print() method on B type => B prints
        anObject = objectC;        // Assign anObject to objectC => Assign Type B to Type C => valid because C extends B (C is subclass from B) => reverse approach not possible!!
        anObject.print();          // Invoke print() method on C type => C prints
    }

    /*
    Output:
    A prints
    B prints
    C prints
     */
}
