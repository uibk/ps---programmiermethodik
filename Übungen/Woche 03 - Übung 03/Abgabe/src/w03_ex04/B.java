package w03_ex04;

public class B extends A {

    /**
     * prints "ex04.B prints"
     */
    public void print() {
        System.out.println("B prints");
    }

    /**
     * prints "B 1"
     *
     * @param a
     */
    public void print(A a) {
        System.out.println("B 1");
    }

    /**
     * prints "B 2"
     *
     * @param b
     */
    public void print(B b) {
        System.out.println("B 2");
    }
}
