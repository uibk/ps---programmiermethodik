package w03_ex01.a;

public class EmployeeTest {

    public static void main(String[] args) {
        Employee employee = new Employee("Gabriel", 20, 40000.00);
        Employee programmer = new Programmer("Fabian", 22, 50000.00, "C");
        Employee designer = new Designer("Laura", 20, 20000.00, "Photoshop");

        employee.printInformation();
        System.out.println("--------------");
        programmer.printInformation();
        System.out.println("--------------");
        designer.printInformation();
        System.out.println("--------------");
    }
}
