package w03_ex01.a;

public class Programmer extends Employee {

    private String programmingLanguage;

    Programmer(String name, int age, double salary, String programmingLanguage) {
        super(name, age, salary);
        this.programmingLanguage = programmingLanguage;
    }

    private String getProgrammingLanguage() {
        return programmingLanguage;
    }

    public void setProgrammingLanguage(String programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
    }

    @Override
    public void printInformation() {
        super.printInformation();
        System.out.println("Programming Language: " + this.getProgrammingLanguage());
    }


}
