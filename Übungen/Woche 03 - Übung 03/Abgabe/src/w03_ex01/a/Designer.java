package w03_ex01.a;

public class Designer extends Employee {

    private String designSofware;

    Designer(String name, int age, double salary, String designSofware) {
        super(name, age, salary);
        this.designSofware = designSofware;
    }

    private String getDesignSofware() {
        return designSofware;
    }

    public void setDesignSofware(String designSofware) {
        this.designSofware = designSofware;
    }

    @Override
    public void printInformation() {
        super.printInformation();
        System.out.println("Design Sofware: " + this.getDesignSofware());
    }
}
