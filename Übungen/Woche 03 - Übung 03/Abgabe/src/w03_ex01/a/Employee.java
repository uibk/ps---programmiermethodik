package w03_ex01.a;

public class Employee {

    private String name;
    private int age;
    private double salary;

    public Employee(String name, int age, double salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public void printInformation() {
        System.out.println(
                "name: " + this.getName()
                        + "\n"
                        + "age: " + this.getAge()
                        + "\n"
                        + "salary: " + this.getSalary());
    }
}
