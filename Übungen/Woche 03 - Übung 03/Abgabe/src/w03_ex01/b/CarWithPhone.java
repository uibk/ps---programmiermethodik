package w03_ex01.b;

public class CarWithPhone extends CarRental {

    public CarWithPhone(Carsize carSize, int numberOfDays) {
        super(carSize, numberOfDays);
    }

    @Override
    public void choosePrice() {
        this.setPrice(super.getCarSize().getPrice() + 10);
    }
}
