package w03_ex01.b;

public enum Carsize {

    ECONOMY(49),
    MEDIUM(55),
    FULLSIZE(65);

    private final double price;

    Carsize(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }
}
