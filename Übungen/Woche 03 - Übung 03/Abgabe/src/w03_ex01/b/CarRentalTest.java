package w03_ex01.b;

public class CarRentalTest {
    public static void main(String[] args) {
        CarRental carRental = new CarRental(Carsize.ECONOMY, 22);
        CarWithPhone carWithPhone = new CarWithPhone(Carsize.ECONOMY, 30);

        carRental.choosePrice();
        carWithPhone.choosePrice();

        carRental.calculatePrice();
        carWithPhone.calculatePrice();

        carRental.printTotalPrice();
        carWithPhone.printTotalPrice();
    }
}
