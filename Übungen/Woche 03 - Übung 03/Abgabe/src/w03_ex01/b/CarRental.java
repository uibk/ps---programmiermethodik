package w03_ex01.b;

public class CarRental {

    private Carsize carSize;
    private double price;
    private double totalPrice;
    private int numberOfDays;

    CarRental(Carsize carSize, int numberOfDays) {
        this.carSize = carSize;
        this.numberOfDays = numberOfDays;
    }

    public void choosePrice() {
        this.price = this.getCarSize().getPrice();
    }

    public void calculatePrice() {
        this.totalPrice = this.price * this.numberOfDays;
    }

    public void printTotalPrice() {
        System.out.println(
                "The total cost is: " + this.totalPrice
        );
    }

    public Carsize getCarSize() {
        return carSize;
    }

    public void setCarSize(Carsize carSize) {
        this.carSize = carSize;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }
}
