import java.util.ArrayList;
import java.util.List;

public class Stock {
    private List<Buyable> stockList;

    public Stock() {
        this.stockList = new ArrayList<>();
    }

    public void addItem(Buyable item) {
        if (!stockList.contains(item)) {
            stockList.add(item);
        }
    }

    public void removeItem(Buyable item) {
        if (stockList.contains(item)) {
            stockList.remove(item);
        }
    }

    public boolean checkOut(ShoppingCart shoppingCart) {
        for (Buyable item :
                shoppingCart.getList()) {
            if (stockList.contains(item)) {
                this.removeItem(item);
            } else {
                return false;
            }
        }
        return true;
    }
}
