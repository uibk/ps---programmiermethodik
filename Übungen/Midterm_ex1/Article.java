public class Article implements Buyable {

    private int id;
    private String name;
    private double price;

    public Article(final int id, final String name, final double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }
}
