import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    private int id;
    private List<Buyable> list;

    public ShoppingCart(final int id) {
        this.id = id;
        this.list = new ArrayList<>();
    }

    public List<Buyable> getList() {
        return list;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void addItem(Buyable item) {
        if (!list.contains(item)) {
            list.add(item);
        }
    }

    public void removeItem(Buyable item) {
        if (list.contains(item)) {
            list.remove(item);
        }
    }

    public double getTotalPrice() {
        double totalPrice = 0.0;
        for (Buyable item :
                this.list) {
            totalPrice += item.getPrice();
        }
        return totalPrice;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Shopping Cart").append(" ").append(this.getId()).append(" ");
        sb.append("(total price: ").append(this.getTotalPrice()).append(")");
        return sb.toString();
    }
}
