package w08_ex02;

public class User {
    private String name;
    private int age;
    private Order order;

    public User(String name, int age) {
        this.name = name;
        this.age = age;
        // Just sample data...
        this.order = new Order("Some items", 1234567);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Order getOrder() {
        return order;
    }
}
