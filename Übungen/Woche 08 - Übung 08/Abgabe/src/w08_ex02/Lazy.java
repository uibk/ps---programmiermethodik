package w08_ex02;

import java.util.function.Supplier;

public class Lazy<T> {

    private T value;
    private boolean initialized;
    private Supplier<T> supplier;

    public Lazy(Supplier<T> supplier) {
        this.supplier = supplier;
        this.initialized = false;
    }

    public T get() {
        if (!initialized) {
            value = supplier.get();
            initialized = true;
        }
        return value;
    }
}
/*
Immutability is important as we only want to access this specific value when needed.
Therefore, no setters are required.
When we would add a setter to this class, we might set a value to the variable which is not initialized already.
We could end up in getting wrong or undefined behaviour when accessing a variable which could be null or empty and should be only accessed when required.
 */
