package w08_ex02;

public class LazyMain {
    private static void printConditional(boolean condition, int thenValue, int elseValue) {
        System.out.println("The answer is: " + (condition ? thenValue : elseValue));
    }

    private static void lazyPrintConditional(boolean condition, Lazy<Integer> thenValue, Lazy<Integer> elseValue) {
        System.out.println("The answer is: " + (condition ? thenValue.get() : elseValue.get()));
    }

    private static int longRunningComputation1() {
        //Do some serious computing for a veeeery long time
        System.out.println("Computation 1 running ...");

        return 42;
    }

    private static int longRunningComputation2() {
        //Do some serious computing for a veeeery long time
        System.out.println("Computation 2 running ...");

        return 41;    //Off by one
    }

    public static void main(String[] args) {
        System.out.println("Pass 1:");
        printConditional(false, longRunningComputation1(), longRunningComputation2());
        System.out.println();

        System.out.println("Pass 2:");
        lazyPrintConditional(true, new Lazy<Integer>(() -> longRunningComputation1()), new Lazy<Integer>(() -> longRunningComputation2()));
    }

}
