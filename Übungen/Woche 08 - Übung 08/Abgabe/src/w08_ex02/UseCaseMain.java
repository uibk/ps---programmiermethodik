package w08_ex02;

public class UseCaseMain {
    public static void main(String[] args) {
        // We don't want to process the order of this user by now
        User user = new User("Andreas", 23);

        // Access data from database only when needed
        Order userOrder = user.getOrder();

        // Process that data
        System.out.println(userOrder);
    }
}
/*
Lazy Loading takes mostly place in web applications and more specific in object-mapped database applications like in mechanism like Hibernate from Java Spring.
There are more concepts which are similarly used like Eager Loading etc.
It is here for preventing the database to load a whole bunch of data when creating an object which stores this data.
We want to ensure that this data is only queried and loaded from the database, when it is really required to return these values.
For this, we make usage of lazy loading. Why should we waste I/O operation and more reserved space at this point of time as whe really need to have.
Lazy Loading works likely as Futures in Java or AJAX in JavaScript. It returns the value of an object only when it is first accessed and therefore required.
The loading process of a database could cost much performance and time and should be used sparely and just when required.
When we are creating new objects which represents a database row which in this case would be a user, we don't want to load all of his products which are stored in one order.
We want to assure that the application returns our required order/orders and appropriate products when we need to process that data. Not before and not after.
 */
