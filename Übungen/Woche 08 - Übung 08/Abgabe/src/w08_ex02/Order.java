package w08_ex02;

import java.util.ArrayList;
import java.util.List;

public class Order {
    private String orderName;
    private int orderNr;
    private List<Product> products;

    public Order(String orderName, int orderNr) {
        this.orderName = orderName;
        this.orderNr = orderNr;
        this.products = initProducts();
    }

    private List<Product> initProducts() {
        ArrayList<Product> products = new ArrayList<>();
        Product product1 = new Product("Smartphone");
        Product product2 = new Product("Chair");
        Product product3 = new Product("Keyboard");

        products.add(product1);
        products.add(product2);
        products.add(product3);
        return products;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public int getOrderNr() {
        return orderNr;
    }

    public void setOrderNr(int orderNr) {
        this.orderNr = orderNr;
    }

    public List<Product> getProducts() {
        return products;
    }

    @Override
    public String toString() {
        return "Order: " + orderName + " " + orderNr + "\nProducts: " + getProducts();
    }
}
