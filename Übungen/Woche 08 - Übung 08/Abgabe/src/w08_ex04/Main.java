package w08_ex04;

import w08_ex03.Citizen;
import w08_ex03.CitizenRepository;
import w08_ex03.RandomCitizenRepository;
import w08_ex03.Statistics;

import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        CitizenRepository repo = createCitizenRepository();
        List<Citizen> citizens = repo.loadCitizens();

        System.out.println(citizens);

        System.out.println("Average age: " + Statistics.getAverageAge(citizens));
        System.out.println("Age histogram: " + Statistics.getAgeHistogram(citizens));
        System.out.println("Average income: " + Statistics.getAverageYearlyIncome(citizens));
        System.out.println("Citizens per occupation: " + Statistics.getCitizensPerOccupation(citizens));
        System.out.println("Avg. income per occupation: " + Statistics.getAvgIncomePerOccupation(citizens));
        System.out.println("Top 5 earners: " + Statistics.getTopEarners(citizens, 5));
    }

    public static CitizenRepository createCitizenRepository() {
        CitizenRepository repository = null;

        System.out.println("Type [C] for CSV and [R] for random:");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();

        switch (input) {
            case "R":
            case "r":
                repository = new RandomCitizenRepository();
                break;
            case "C":
            case "c":
                System.out.println("Type in path from file:");
                String path = scanner.next();
                repository = new CsvCitizenRepository(Paths.get(path));
                break;
        }
        scanner.close();

        return repository;
    }
}


