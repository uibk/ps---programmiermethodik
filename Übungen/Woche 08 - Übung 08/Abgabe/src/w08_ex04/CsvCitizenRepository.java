package w08_ex04;

import w08_ex03.Citizen;
import w08_ex03.CitizenRepository;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CsvCitizenRepository implements CitizenRepository {

    private final String DELIMITER = ",";
    private Path path;

    public CsvCitizenRepository(Path path) {
        this.path = path;
    }

    @Override
    public List<Citizen> loadCitizens() {
        List<Citizen> citizens = new ArrayList<>();
        // Since Java 7 file streams are closed automatically when declaring in try-block
        try (
                BufferedReader bufferedReader = new BufferedReader(new FileReader(path.toString()));
                Scanner scanner = new Scanner(bufferedReader)
        ) {
            scanner.useDelimiter(DELIMITER);
            while (scanner.hasNext()) {
                String[] citizenDetail = scanner.nextLine().split(DELIMITER);
                if (citizenDetail.length < 5) {
                    throw new IllegalArgumentException("You must specify 5 columns in your CSV file!");
                }
                citizens.add(new Citizen(citizenDetail[0], citizenDetail[1], Integer.parseInt(citizenDetail[2]), citizenDetail[3], Double.parseDouble(citizenDetail[4])));
            }
        } catch (FileNotFoundException e) {
            System.out.println("File does not exist!");
            return null;
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return citizens;
    }
}
