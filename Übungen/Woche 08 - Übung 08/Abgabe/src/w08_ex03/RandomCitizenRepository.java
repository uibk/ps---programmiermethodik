package w08_ex03;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * A repository which randomly generates 100 citizens.
 */
public class RandomCitizenRepository implements CitizenRepository {
    private static final String[] occupations = {"Programmer", "Student", "Professor", "Office Worker", "Retail Worker", "Doctor"};

    /**
     * Loads 100 random citizens.
     */
    @Override
    public List<Citizen> loadCitizens() {
        List<Citizen> result = new ArrayList<Citizen>();
        RandomNameGenerator rng = new RandomNameGenerator();
        Random rand = new Random();

        for (int i = 0; i < 100; i++) {
            String first = rng.getRandomFirstname();
            String last = rng.getRandomLastname();
            int age = rand.nextInt(100);
            String occupation = occupations[rand.nextInt(occupations.length)];
            double income = rand.nextDouble() * 5000 * 12;

            result.add(new Citizen(first, last, age, occupation, income));
        }

        return result;
    }
}
