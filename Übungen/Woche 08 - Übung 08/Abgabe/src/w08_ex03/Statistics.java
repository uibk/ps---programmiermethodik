package w08_ex03;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A class for calculating various citizen statistics.
 */
public class Statistics {
    private static void CheckIfNullOrEmpty(List<Citizen> citizens) {
        if (citizens == null || citizens.size() < 1) {
            throw new IllegalArgumentException("citizens may not be null or empty");
        }
    }

    /**
     * Calculates the average age of all citizens in the given list.
     *
     * @param citizens The list of citizens for which the average age should by calculated.
     * @return The average age.
     */
    public static double getAverageAge(List<Citizen> citizens) {
        CheckIfNullOrEmpty(citizens);

        return citizens.stream().mapToDouble(Citizen::getAge).average().orElse(Double.NaN);
    }

    /**
     * Calculates the average income of all citizens in the given list.
     *
     * @param citizens The list of citizens for which the average income should by calculated.
     * @return The average income.
     */
    public static double getAverageYearlyIncome(List<Citizen> citizens) {
        CheckIfNullOrEmpty(citizens);

        return citizens.stream().mapToDouble(Citizen::getYearlyIncome).average().orElse(Double.NaN);
    }

    /**
     * Calculates the number of citizens per occupation.
     *
     * @param citizens The list of citizens.
     * @return The number of citizens per occupation, as a Map<String, Integer>.
     */
    public static Map<String, Long> getCitizensPerOccupation(List<Citizen> citizens) {
        CheckIfNullOrEmpty(citizens);

        return citizens.stream().collect(Collectors.groupingBy(Citizen::getOccupation, Collectors.counting()));
    }

    /**
     * Calculates the average income per occupation.
     *
     * @param citizens The list of citizens.
     * @return The average income per occupation, as a Map<String, Double>.
     */
    public static Map<String, Double> getAvgIncomePerOccupation(List<Citizen> citizens) {
        CheckIfNullOrEmpty(citizens);
//        //Count citizens per occupation
//        Map<String, Long> counts = getCitizensPerOccupation(citizens);
//        citizens.stream()
//                .distinct()
//                .map(Citizen::getOccupation)
//                .forEach((occupation -> result.put(occupation,
//                        citizens.stream()
//                                .filter(citizen -> citizen.getOccupation().equals(occupation))
//                                .mapToDouble(Citizen::getYearlyIncome)
//                                //Sum income per occupation
//                                .reduce(Double::sum)
//                                .getAsDouble()
//                                //Divide to get average
//                                / counts.get(occupation))
//                ));
        return citizens.stream().collect(Collectors.groupingBy(Citizen::getOccupation, Collectors.averagingDouble(Citizen::getYearlyIncome)));
    }

    /**
     * Calculates an age histogram for the given list of citizens.
     *
     * @param citizens The list of citizens.
     * @return An age histogram, as a Map<String, Integer>, where the String represents an age group in intervals of 5 years.
     */
    public static Map<String, Long> getAgeHistogram(List<Citizen> citizens) {
        CheckIfNullOrEmpty(citizens);

        return citizens.stream().collect(Collectors.groupingBy((Citizen citizen) ->
                ((citizen.getAge() / 5) * 5) + " to " + (((citizen.getAge() / 5) * 5) + 4), Collectors.counting()));
    }

    /**
     * Calculates the n top earners in the given list of citizens.
     *
     * @param citizens The list of citizens.
     * @param n        How many top earners should be retrieved.
     * @return The List of the n top earners, sorted in descending order of income.
     */
    public static List<Citizen> getTopEarners(List<Citizen> citizens, int n) {
        CheckIfNullOrEmpty(citizens);
        if (citizens.size() < n) {
            throw new IllegalArgumentException("citizens");
        }

        return citizens.stream().sorted(Comparator.comparingDouble(Citizen::getYearlyIncome).reversed()).limit(n).collect(Collectors.toList());
    }
}
