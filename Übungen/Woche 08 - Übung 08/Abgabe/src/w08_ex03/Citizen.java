package w08_ex03;

/**
 * A single citizen.
 */
public class Citizen {
    private String firstname;
    private String lastname;
    private int age;
    private String occupation;
    private double yearlyIncome;

    public Citizen(String firstname, String lastname, int age, String occupation, double yearlyIncome) {
        super();
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.occupation = occupation;
        this.yearlyIncome = yearlyIncome;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public double getYearlyIncome() {
        return yearlyIncome;
    }

    public void setYearlyIncome(double yearlyIncome) {
        this.yearlyIncome = yearlyIncome;
    }

    @Override
    public String toString() {
        return this.firstname + " " + this.lastname + " (age: " + this.age + ", yearly income:  " + this.yearlyIncome + ", occupation: " + this.occupation + ")";
    }
}
