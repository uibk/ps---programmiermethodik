package w08_ex03;

import java.util.List;

/**
 * A repository for retrieving citizens.
 *
 * @author maximilian
 */
public interface CitizenRepository {
    /**
     * Load citizens from the repository.
     *
     * @return The list of citizens.
     */
    List<Citizen> loadCitizens();
}
