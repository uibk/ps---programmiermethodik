package w08_ex03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * ArrayStack class for Statistics.
 */
public class StaticticsTest {
    @Test
    public void getAverageAgeNormal() {
        List<Citizen> l = new ArrayList<Citizen>();
        l.add(new Citizen("", "", 10, "", 0.0));
        l.add(new Citizen("", "", 20, "", 0.0));
        l.add(new Citizen("", "", 30, "", 0.0));
        double avgAge = Statistics.getAverageAge(l);

        Assertions.assertEquals(avgAge, 20.0);
    }

    @Test
    public void getAverageIncomeNormal() {
        List<Citizen> l = new ArrayList<Citizen>();
        l.add(new Citizen("", "", 0, "", 10.0));
        l.add(new Citizen("", "", 0, "", 20.0));
        l.add(new Citizen("", "", 0, "", 30.0));
        double avgAge = Statistics.getAverageYearlyIncome(l);

        Assertions.assertEquals(avgAge, 20.0);
    }

    @Test
    public void getCitizensPerOccupationNormal() {
        List<Citizen> l = new ArrayList<Citizen>();
        l.add(new Citizen("", "", 0, "Student", 0.0));
        l.add(new Citizen("", "", 0, "Student", 0.0));
        l.add(new Citizen("", "", 0, "Doctor", 0.0));
        Map<String, Long> res = Statistics.getCitizensPerOccupation(l);

        Assertions.assertEquals(res.size(), 2);
        Assertions.assertEquals((long) res.get("Student"), 2L);
        Assertions.assertEquals((long) res.get("Doctor"), 1L);
    }

    @Test
    public void getAvgIncomePerOccupationNormal() {
        List<Citizen> l = new ArrayList<Citizen>();
        l.add(new Citizen("", "", 0, "Student", 100.0));
        l.add(new Citizen("", "", 0, "Student", 200.0));
        l.add(new Citizen("", "", 0, "Doctor", 1000.0));
        Map<String, Double> res = Statistics.getAvgIncomePerOccupation(l);

        Assertions.assertEquals(res.size(), 2);
        Assertions.assertEquals((double) res.get("Student"), 150.0);
        Assertions.assertEquals((double) res.get("Doctor"), 1000.0);
    }

    @Test
    public void getAgeHistogramNormal() {
        List<Citizen> l = new ArrayList<Citizen>();
        l.add(new Citizen("", "", 17, "", 0.0));
        l.add(new Citizen("", "", 18, "", 0.0));
        l.add(new Citizen("", "", 22, "", 0.0));
        l.add(new Citizen("", "", 21, "", 0.0));
        l.add(new Citizen("", "", 24, "", 0.0));
        l.add(new Citizen("", "", 66, "", 0.0));
        Map<String, Long> res = Statistics.getAgeHistogram(l);

        Assertions.assertEquals(res.size(), 3);
        Assertions.assertEquals((long) res.get("15 to 19"), 2L);
        Assertions.assertEquals((long) res.get("20 to 24"), 3L);
        Assertions.assertEquals((long) res.get("65 to 69"), 1L);
    }

    @Test
    public void getTopEarnersNormal() {
        List<Citizen> l = new ArrayList<Citizen>();
        l.add(new Citizen("A", "", 0, "", 1000.0));
        l.add(new Citizen("B", "", 0, "", 200.0));
        l.add(new Citizen("C", "", 0, "", 1300.0));
        l.add(new Citizen("D", "", 0, "", 160.0));
        l.add(new Citizen("E", "", 0, "", 0.0));
        l.add(new Citizen("F", "", 0, "", 1100.0));
        List<Citizen> res = Statistics.getTopEarners(l, 3);

        Assertions.assertEquals(res.size(), 3);
        Assertions.assertEquals(res.get(0).getFirstname(), "C");
        Assertions.assertEquals(res.get(1).getFirstname(), "F");
        Assertions.assertEquals(res.get(2).getFirstname(), "A");
    }

    @Test
    public void getAverageAgeNull() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Statistics.getAverageAge(null);
        });
    }

    @Test
    public void getAverageIncomeNull() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Statistics.getAverageYearlyIncome(null);
        });
    }

    @Test
    public void getCitizensPerOccupationNull() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Statistics.getCitizensPerOccupation(null);
        });
    }

    @Test
    public void getAvgIncomePerOccupationNull() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Statistics.getAvgIncomePerOccupation(null);
        });
    }

    @Test
    public void getAgeHistogramNull() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Statistics.getAgeHistogram(null);
        });
    }

    @Test
    public void getTopEarnersNull() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Statistics.getTopEarners(null, 3);
        });
    }

    @Test
    public void getAverageAgeEmpty() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Statistics.getAverageAge(new ArrayList<Citizen>());
        });
    }

    @Test
    public void getAverageIncomeEmpty() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Statistics.getAverageYearlyIncome(new ArrayList<Citizen>());
        });
    }

    @Test
    public void getCitizensPerOccupationEmpty() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Statistics.getCitizensPerOccupation(new ArrayList<Citizen>());
        });
    }

    @Test
    public void getAvgIncomePerOccupationEmpty() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Statistics.getAvgIncomePerOccupation(new ArrayList<Citizen>());
        });
    }

    @Test
    public void getAgeHistogramEmpty() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Statistics.getAgeHistogram(new ArrayList<Citizen>());
        });
    }

    @Test
    public void getTopEarnersEmpty() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Statistics.getTopEarners(new ArrayList<Citizen>(), 3);
        });
    }

    @Test
    public void getTopEarnersTooSmall() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            List<Citizen> l = new ArrayList<Citizen>();
            l.add(new Citizen("", "", 0, "", 0.0));
            Statistics.getTopEarners(l, 3);
        });
    }
}
