package w08_ex01;

import java.math.BigDecimal;
import java.math.RoundingMode;

public interface Summable {
    double getSum();

    default double getSumRounded() {
        BigDecimal bd = new BigDecimal(Double.toString(getSum()));
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
