package w06_ex03;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ListStatisticsTest {
    @Test
    public void averageCalculation() {
        List<Integer> values = new ArrayList<>();

        values.add(0);
        values.add(1);
        values.add(2);

        double avg = ListStatistics.average(values);
        assertEquals(1.0, avg);
    }

    @Test
    void emptyList() {
        assertThrows(IllegalArgumentException.class, () -> ListStatistics.average(null));
    }

    @Test
    void nullEntryList() {
        List<Integer> list = new LinkedList<>();

        list.add(null);
        list.add(null);
        list.add(null);

        assertThrows(IllegalArgumentException.class, () -> ListStatistics.average(list));
    }

    @Test
    void partialNullList() {
        List<Integer> list = new LinkedList<>();

        list.add(20);
        list.add(70);
        list.add(null);
        list.add(200);
        list.add(null);
        list.add(null);

        double average = (20 + 70 + 200) / 3;

        assertEquals(average, ListStatistics.average(list));
    }
}
