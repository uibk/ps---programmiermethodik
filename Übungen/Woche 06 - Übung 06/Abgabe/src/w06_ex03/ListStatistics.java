package w06_ex03;

import java.util.List;

/**
 * Class implementing statistical functions for lists.
 */
public class ListStatistics {

    /**
     * This method calculates the average of a list of integer values.
     *
     * @param list a list of integer values, null values are ignored and do not count as values.
     * @return the average of all values (null values are ignored and not counted).
     */
    public static double average(List<Integer> list) throws IllegalArgumentException {
        int sum = 0, entryCounter = 0;
        if (list.get(0) == null) {
            throw new IllegalArgumentException("List starts with null");
        }
        if (list.isEmpty()) {
            throw new IllegalArgumentException("List is empty!");
        }
        for (Integer entry : list) {
            if (entry != null) {
                sum += entry;
                entryCounter++;
            }
        }
        if (entryCounter == 0) {
            throw new IllegalArgumentException("List has either no elements or all elements are empty!");
        }
        return sum / entryCounter;
    }
}
