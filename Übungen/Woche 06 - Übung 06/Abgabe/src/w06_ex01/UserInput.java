package w06_ex01;

import java.util.Scanner;

public class UserInput {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input = "";
        do {
            try {
                System.out.print("Please input a number: ");
                input = scanner.nextLine();
                int number = Integer.parseInt(input);
                System.out.println("x  = " + number);
                System.out.println("x² = " + (number * number));
            } catch (NumberFormatException e) {
                System.out.println(input + " is not an integer!");
            }
        } while (!input.matches("[0-9]+"));


        scanner.close();
    }
}
