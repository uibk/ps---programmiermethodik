package w06_ex02;

public class BasicProduct implements Product {
    private String name;
    private double price;

    public BasicProduct(String name, double price) {
        if (name == null || price <= 0) {
            throw new IllegalArgumentException();
        } else {
            this.name = name;
            this.price = price;
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }
}
