package w06_ex02;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ProductManagerTest {
    @Test
    void negativeSize() {
        assertThrows(IllegalArgumentException.class, () -> new ProductManager(-5));
    }

    @Test
    void incrementedSize() {
        ProductManager productManager = new ProductManager(3);
        try {
            productManager.addProduct(new BasicProduct("Chair", 50));
            productManager.addProduct(new BasicProduct("Keyboard", 150));
        } catch (StoreFullException e) {
            e.printStackTrace();
        }
        assertEquals(2, productManager.getSize());
    }

    @Test
    void storedProduct() {
        ProductManager productManager = new ProductManager(3);
        try {
            productManager.addProduct(new BasicProduct("Chair", 50));
        } catch (StoreFullException e) {
            e.printStackTrace();
        }
        assertEquals("Chair", productManager.getProducts().get(0).getName());
    }

    @Test
    void maximumStore() {
        ProductManager productManager = new ProductManager(3);
        try {
            productManager.addProduct(new BasicProduct("Chair", 50));
            productManager.addProduct(new BasicProduct("Desk", 70));
            productManager.addProduct(new BasicProduct("Desk", 100));
        } catch (StoreFullException e) {
            e.printStackTrace();
        }
        assertEquals(3, productManager.getSize());
        assertThrows(StoreFullException.class, () -> productManager.addProduct(new BasicProduct("Mouse", 90)));
    }

    @Test
    void removeProduct() {
        ProductManager productManager = new ProductManager(2);
        Product chair = new BasicProduct("Char", 50);
        try {
            productManager.addProduct(chair);
        } catch (StoreFullException e) {
            e.printStackTrace();
        }
        productManager.removeProduct(chair);
        assertEquals(0, productManager.getProducts().size());
    }

    @Test
    void totalPrice() {
        ProductManager productManager = new ProductManager(3);
        Product chair = new BasicProduct("Char", 50);
        Product desk = new BasicProduct("Desk", 150);
        Product mouse = new BasicProduct("Mouse", 100);
        try {
            productManager.addProduct(chair);
            productManager.addProduct(desk);
            productManager.addProduct(mouse);
        } catch (StoreFullException e) {
            e.printStackTrace();
        }
        double totalPrice = chair.getPrice() + desk.getPrice() + mouse.getPrice();
        assertEquals(totalPrice, productManager.getTotalPrice());
    }
}