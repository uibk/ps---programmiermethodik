package w06_ex02;

public class StoreFullException extends Exception {
    public StoreFullException(String message) {
        super(message);
    }
}
