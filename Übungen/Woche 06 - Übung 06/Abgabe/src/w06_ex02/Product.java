package w06_ex02;

/**
 * This interface represents a product.
 */
public interface Product {

    /**
     * @return the name of a given product.
     */
    public String getName();

    /**
     * @return the price of a given product.
     */
    public double getPrice();
}
