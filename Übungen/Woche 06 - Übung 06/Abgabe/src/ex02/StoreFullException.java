package ex02;

public class StoreFullException extends Exception {
    public StoreFullException(String message) {
        super(message);
    }
}
