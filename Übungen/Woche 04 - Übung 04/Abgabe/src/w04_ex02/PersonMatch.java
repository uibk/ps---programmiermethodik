package w04_ex02;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * This class stores information about a person and its related persons.
 * 
 * @author Alexander Blaas
 *
 */
public class PersonMatch {

	private Person person;
	private List<Person> relatedPersons;

	public PersonMatch(Person person) {
		super();
		this.person = person;
	}

	/**
	 * @param relatedPersons the relatedPersons to set
	 */
	public void setRelatedPersons(List<Person> relatedPersons) {
		this.relatedPersons = relatedPersons;
	}

	/**
	 * @return the person
	 */
	public Person getPerson() {
		return person;
	}

	/**
	 * @return the relatedPersons
	 */
	public Collection<Person> getRelatedPersons() {
		return Collections.unmodifiableCollection(this.relatedPersons);
	}

}
