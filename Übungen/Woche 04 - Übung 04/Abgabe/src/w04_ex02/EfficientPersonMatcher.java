package w04_ex02;

import java.util.*;

public class EfficientPersonMatcher implements PersonMatcher {

    private Map<String, Person> firstGroup = new HashMap<>();

    @Override

    public void initGroup(List<Person> firstGroup) {
        firstGroup.forEach((Person person) -> this.firstGroup.put(person.getLastName(), person));
    }

    @Override
    public Collection<PersonMatch> findAllRelatedPersons(List<Person> secondGroup) {
        List<PersonMatch> matchesToReturn = new LinkedList<>();
        for (Person pSecondGroup : secondGroup) {
            List<Person> relatedPersons = new LinkedList<>();
            PersonMatch pMatchEntry = new PersonMatch(pSecondGroup);
            firstGroup.forEach((key, value) -> {
                if (pSecondGroup.getLastName().equalsIgnoreCase(key)) {
                    relatedPersons.add(value);
                }
            });
            pMatchEntry.setRelatedPersons(relatedPersons);
            matchesToReturn.add(pMatchEntry);
        }
        return matchesToReturn;
    }
}
