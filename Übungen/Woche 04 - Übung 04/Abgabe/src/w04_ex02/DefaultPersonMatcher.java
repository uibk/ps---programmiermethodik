package w04_ex02;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DefaultPersonMatcher implements PersonMatcher {

	private List<Person> firstGroup = new ArrayList<>();


	@Override
	public void initGroup(List<Person> firstGroup) {
		this.firstGroup.addAll(firstGroup);
	}

	@Override
	public Collection<PersonMatch> findAllRelatedPersons(List<Person> secondGroup) {
		List<PersonMatch> matchesToReturn = new ArrayList<>();
		for (Person pSecondGroup : secondGroup) {
			List<Person> relatedPersons = new ArrayList<>();
			PersonMatch pMatchEntry = new PersonMatch(pSecondGroup);
			for (Person pFirstGroup : this.firstGroup) {
				if (pSecondGroup.getLastName().equalsIgnoreCase(pFirstGroup.getLastName())) {
					relatedPersons.add(pFirstGroup);
				}
			}
			pMatchEntry.setRelatedPersons(relatedPersons);
			matchesToReturn.add(pMatchEntry);
		}
		return matchesToReturn;
	}

}
