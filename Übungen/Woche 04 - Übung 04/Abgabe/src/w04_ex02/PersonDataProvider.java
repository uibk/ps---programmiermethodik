package w04_ex02;

import java.util.LinkedList;
import java.util.List;

/**
 * This class provides randomly generated persons
 * 
 * @author Alexander Blaas
 *
 */
public class PersonDataProvider {

	private RandomNameGenerator nameGenerator = new RandomNameGenerator();

	/**
	 * Generates randomly persons
	 * 
	 * @param amount The number of persons
	 * @return A list containing randomly generated persons
	 */
	public List<Person> providePersons(long amount) {
		List<Person> persons = new LinkedList<>();
		for (int i = 0; i < amount; i++) {
			persons.add(new Person(i, nameGenerator.getRandomFirstname(), nameGenerator.getRandomLastname()));
		}
		return persons;
	}

}
