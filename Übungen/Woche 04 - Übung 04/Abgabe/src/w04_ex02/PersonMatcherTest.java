package w04_ex02;

import java.util.Collection;
import java.util.List;

public class PersonMatcherTest {

	private static final long PERSONS_PER_GROUP = 10000;

	private static void evaluateMatcher(PersonMatcher initializedPersonMatcher, List<Person> secondGroup) {
		long startTime = System.currentTimeMillis();
		Collection<PersonMatch> allRelations = initializedPersonMatcher.findAllRelatedPersons(secondGroup);
		long endTime = System.currentTimeMillis();

		System.out.println("Used implementation: " + initializedPersonMatcher.getClass().getSimpleName());
		System.out.println("Relations processed: " + allRelations.size());
		System.out.println("Elapsed time: " + (endTime - startTime) + " ms\n");
	}

	public static void main(String[] args) {

		PersonMatcher defaultPersonMatcher = new DefaultPersonMatcher();
		PersonMatcher efficientPersonMatcher = new EfficientPersonMatcher();

		PersonDataProvider dataProvider = new PersonDataProvider();

		List<Person> firstGroup = dataProvider.providePersons(PERSONS_PER_GROUP);
		List<Person> secondGroup = dataProvider.providePersons(PERSONS_PER_GROUP);

		defaultPersonMatcher.initGroup(firstGroup);
		efficientPersonMatcher.initGroup(firstGroup);

		evaluateMatcher(defaultPersonMatcher, secondGroup);
		evaluateMatcher(efficientPersonMatcher, secondGroup);
	}
}
