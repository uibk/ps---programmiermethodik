package w04_ex02;

/**
 * This class contains information about a person.
 * 
 * @author Alexander Blaas
 *
 */
public class Person {

	private int id;
	private String firstName;
	private String lastName;

	public Person(int id, String firstName, String lastName) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.firstName+" "+this.lastName+" (id="+this.id+")";
	}

	
}
