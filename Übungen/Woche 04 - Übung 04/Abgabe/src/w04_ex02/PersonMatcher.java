package w04_ex02;

import java.util.Collection;
import java.util.List;

/**
 * Specifies fundamental methods to realize a personMatcher.
 * 
 * @author Alexander Blaas
 *
 */
public interface PersonMatcher {

	/**
	 * This method is used to initialize a group of persons, i.e. to store relevant
	 * information into adequate collections. This method needs to be called before
	 * the next one is called.
	 * 
	 * @param firstGroup The first group of persons.
	 */
	public void initGroup(List<Person> firstGroup);

	/**
	 * Iterates over the second group (which is provided as a parameter for this
	 * method) and finds for every person within the second group all the related
	 * persons which are within the first group (the group you set before).
	 * "Related" means having the same lastname.
	 * 
	 * @param secondGroup The second group, i.e. the group for which it is required
	 *                    to find related persons.
	 * 
	 * @return A collection where each entry contains the person and its related
	 *         persons from the second group.
	 */
	public Collection<PersonMatch> findAllRelatedPersons(List<Person> secondGroup);

}
