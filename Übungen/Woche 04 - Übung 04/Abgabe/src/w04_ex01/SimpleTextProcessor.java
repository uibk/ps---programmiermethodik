package w04_ex01;

import java.util.Map;
import java.util.TreeMap;

public class SimpleTextProcessor implements TextProcessor {
    private Map<String, Integer> wordMap = new TreeMap<>();

    public void processText(String text) {
        String[] words = text.toLowerCase().split(" "); // save it in lower case for proper sorting
        for (String word :
                words) {
            wordMap.merge(word, 1, Integer::sum);
        }
    }

    @Override
    public long getWordCount(String word) {
        return wordMap.get(word);
    }

    @Override
    public void printStatistics() {
        for (String key :
                wordMap.keySet()) {
            System.out.println("Word: [" + key + "] Occurrences: [" + getWordCount(key) + "]");
        }
    }
}
