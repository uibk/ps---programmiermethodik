package w04_ex01;

/**
 * Specifies required methods for our wordCount implementation.
 *
 * @author Alexander Blaas
 */
public interface TextProcessor {

    void processText(String text);

    long getWordCount(String word);

    void printStatistics();

}