package w04_ex01;

public class WordCountTest {
    public static void main(String[] args) {
        TextProcessor simpleText = new SimpleTextProcessor();
        simpleText.processText(TextContainer.TEXT);
        simpleText.printStatistics();
    }
}
