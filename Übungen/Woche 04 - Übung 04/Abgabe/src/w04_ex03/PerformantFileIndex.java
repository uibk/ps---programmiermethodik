package w04_ex03;

import w04_ex02.Person;

import java.util.*;

public class PerformantFileIndex extends FileIndex {

    private Map<String, CustomFileEntry> index;

    public PerformantFileIndex(CustomDirectory rootFile) {
        super(rootFile);
        index = new HashMap<>();
    }

    @Override
    protected void addFileToIndex(CustomFileEntry entry) {
        if (!fileExists(entry.getPath())) {
            index.put(entry.getPath(), entry);
        }
    }

    @Override
    public boolean fileExists(String path) {
        return index.containsKey(path);
    }

    @Override
    public CustomFileEntry findFile(String path) {
        for (Map.Entry<String, CustomFileEntry> entry :
                index.entrySet()) {
            if (entry.getKey().equals(path)) {
                return entry.getValue();
            }
        }
        return null;
    }

    @Override
    public Collection<CustomFileEntry> findFilesByType(String fileType) {
        List<CustomFileEntry> fileList = new ArrayList<>();
        index.forEach((key, value) -> {
            if (value.getFileType().equals(fileType)) {
                fileList.add(value);
            }
        });
        return fileList;
    }

    @Override
    public Collection<CustomFileEntry> getFilesByOwner(Person owner) {
        List<CustomFileEntry> fileList = new ArrayList<>();
        index.forEach((key, value) -> {
            if (value.getOwner().equals(owner)) {
                fileList.add(value);
            }
        });
        return fileList;
    }

    @Override
    public Collection<CustomFileEntry> getPermittedFiles(Person user) {
        List<CustomFileEntry> fileList = new ArrayList<>();
        index.forEach((key, value) -> {
            for (Person permittedUser :
                    value.getPermittedUsers()) {
                if (permittedUser.equals(user)) {
                    fileList.add(value);
                }
            }
        });
        return fileList;
    }

    @Override
    public Collection<CustomFileEntry> getPermittedFilesOfType(Person user, String fileType) {
        List<CustomFileEntry> fileList = new ArrayList<>();
        index.forEach((key, value) -> {
            for (Person permittedUser :
                    value.getPermittedUsers()) {
                if (permittedUser.equals(user) && value.getFileType().equals(fileType)) {
                    fileList.add(value);
                }
            }
        });
        return fileList;
    }

    @Override
    public Collection<CustomFileEntry> sortFilesByPath(Collection<CustomFileEntry> toSort) {
        List<CustomFileEntry> sortedFiles = new ArrayList<>(toSort);
        Collections.sort(sortedFiles);
        return sortedFiles;
    }

    @Override
    public Collection<CustomFileEntry> sortFilesByCreationDate(Collection<CustomFileEntry> toSort) {
        List<CustomFileEntry> sortedFiles = new ArrayList<>(toSort);
        sortedFiles.sort(Comparator.comparing(CustomFileEntry::getCreationDate));
        return sortedFiles;
    }
}
