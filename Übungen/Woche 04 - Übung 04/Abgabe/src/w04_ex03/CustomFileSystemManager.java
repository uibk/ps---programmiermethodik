package w04_ex03;

import w04_ex02.Person;

import java.util.Collection;

/**
 * Specifies search-, respectively, sorting methods to apply on a filesystem.
 *
 * @author Alexander Blaas
 */
public interface CustomFileSystemManager {

    /**
     * Performs initialization if required.
     */
    public void doInit();

    /**
     * Checks if a file exists at given path.
     *
     * @param path The given path.
     * @return True if the file exists, else otherwise.
     */
    public boolean fileExists(String path);

    /**
     * Finds a file according to its path.
     *
     * @param path The given path.
     * @return The file if found, null otherwise.
     */
    public CustomFileEntry findFile(String path);

    /**
     * Returns all files of a given type.
     *
     * @param fileType The given type.
     * @return All files of that type.
     */
    public Collection<CustomFileEntry> findFilesByType(String fileType);

    /**
     * Returns all files of a given owner.
     *
     * @param owner The given owner.
     * @return All files of the given owner.
     */
    public Collection<CustomFileEntry> getFilesByOwner(Person owner);

    /**
     * Returns all files the given user has permissions.
     *
     * @param user The given user.
     * @return All files a user has permissions.
     */
    public Collection<CustomFileEntry> getPermittedFiles(Person user);

    /**
     * Returns all files of a certain type the given user has permissions.
     *
     * @param user     The given user.
     * @param fileType The given filetype.
     * @return All files of a certain type a user has permissions.
     */
    public Collection<CustomFileEntry> getPermittedFilesOfType(Person user, String fileType);

    /**
     * Sorts files by their path in ascending order.
     *
     * @param toSort The files to sort.
     * @return The files sorted by path.
     */
    public Collection<CustomFileEntry> sortFilesByPath(Collection<CustomFileEntry> toSort);

    /**
     * Sorts files by their creation-date in descending order.
     *
     * @param toSort The files to sort.
     * @return The files sorted by creation-date.
     */
    public Collection<CustomFileEntry> sortFilesByCreationDate(Collection<CustomFileEntry> toSort);
}
