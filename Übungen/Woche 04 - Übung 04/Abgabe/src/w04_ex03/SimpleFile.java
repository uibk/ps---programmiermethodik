package w04_ex03;

/**
 * A simple file.
 *
 * @author Alexander Blaas
 */
public class SimpleFile extends CustomFileEntry {

    private String fileType;

    public SimpleFile(String name, CustomDirectory parent, String fileType) {
        super(name, parent);
        this.fileType = fileType;
    }

    @Override
    public String getPath() {
        return super.getPath() + "." + this.fileType;
    }

    /**
     * @return the fileType
     */
    @Override
    public String getFileType() {
        return fileType;
    }

    /**
     * @param fileType the fileType to set
     */
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
