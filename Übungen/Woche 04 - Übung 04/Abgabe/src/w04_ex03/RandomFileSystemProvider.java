package w04_ex03;

import w04_ex02.Person;
import w04_ex02.PersonDataProvider;

import java.util.*;

/**
 * An imnplementation of CustomFileSystemProvider: It generates a random file
 * system.
 *
 * @author Alexander Blaas
 */
public class RandomFileSystemProvider implements CustomFileSystemProvider {

    private static final String DIR_SUFFIX = "directory";
    private static final String FILE_SUFFIX = "file";
    private static final String[] FILE_TYPES = {"pdf", "xls", "xlsx", "doc", "txt", "java", "exe", "docx", "jpg", "png", "gif"};

    private int nrOfDifferentPersons;
    private int maxNrOfSubDirectories;
    private int maxNrOfFilesPerDirectory;

    private PersonDataProvider personDataProvider = new PersonDataProvider();
    private List<Person> randomPersons;

    public RandomFileSystemProvider(int nrOfDifferentPersons, int maxNrOfSubDirectories, int maxNrOfFilesPerDirectory) {
        super();
        this.nrOfDifferentPersons = nrOfDifferentPersons;
        this.maxNrOfSubDirectories = maxNrOfSubDirectories;
        this.maxNrOfFilesPerDirectory = maxNrOfFilesPerDirectory;
    }

    @Override
    public void doInit() {
        this.initRandomPersons();
    }

    @Override
    public List<String> getFileTypes() {
        return Arrays.asList(FILE_TYPES);
    }

    @Override
    public Collection<Person> getPersonList() {
        return Collections.unmodifiableCollection(this.randomPersons);
    }

    /**
     * Just creates randomly persons (owners, permitted users).
     */
    private void initRandomPersons() {
        this.randomPersons = this.personDataProvider.providePersons(this.nrOfDifferentPersons);
    }

    /**
     * Creates random creation dates.
     *
     * @return A randomly created creation date.
     */
    private Date createRandomDate() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        calendar.add(Calendar.MONTH, new Random().nextInt(23) * -1);
        calendar.add(Calendar.DAY_OF_MONTH, new Random().nextInt(67) * -1);
        calendar.add(Calendar.YEAR, new Random().nextInt(4) * -1);

        return calendar.getTime();
    }

    /**
     * Returns a random person out of the random-person-pool.
     *
     * @return A random person.
     */
    private Person getRandomPerson() {
        return this.randomPersons.get(new Random().nextInt(this.nrOfDifferentPersons));
    }

    /**
     * Randomly adds permissions to a file.
     *
     * @param entry The file to add permissions for.
     */
    private void addRandomPermissions(CustomFileEntry entry) {
        long nrOfPermissions = new Random().nextInt(this.nrOfDifferentPersons);

        for (int i = 0; i < nrOfPermissions; i++) {
            entry.addPermission(this.getRandomPerson());
        }
    }

    /**
     * Returns a random file-type out of a given list of file-types.
     *
     * @return A random file-type.
     */
    private String getRandomFileType() {
        return FILE_TYPES[new Random().nextInt(FILE_TYPES.length)];
    }

    /**
     * Creates a simple file entry.
     *
     * @param name     The files' name.
     * @param fileType The file type.
     * @param parent   Its parent.
     * @return A simple file entry.
     */
    private SimpleFile createSimpleFile(String name, String fileType, CustomDirectory parent) {
        return new SimpleFile(name, parent, fileType);
    }

    /**
     * Creates a custom directory entry.
     *
     * @param name   Its name.
     * @param parent Its parent.
     * @return A directory entry.
     */
    private CustomDirectory createCustomDirectory(String name, CustomFileEntry parent) {
        return new CustomDirectory(name, parent);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.exercise.file.system.data.CustomFileSystemProvider#
     * provideRandomFileSystem(int)
     */
    @Override
    public CustomDirectory provideFileSystem(int nrOfHierarchyLevels, String rootDirName) {
        /* Create root directory & set properties */
        CustomDirectory rootDirectory = this.createCustomDirectory(rootDirName, null);
        rootDirectory.setOwner(this.getRandomPerson());
        this.addRandomPermissions(rootDirectory);
        rootDirectory.setCreationDate(this.createRandomDate());

        int nrOfSubDirectories = new Random().nextInt(this.maxNrOfSubDirectories);
        int nrOfFiles = new Random().nextInt(this.maxNrOfFilesPerDirectory);
        int actualHierarchyLevel = 1;

        /* Recursively create subdirectories */
        this.setupFileHierarchy(rootDirectory, nrOfSubDirectories, nrOfFiles, actualHierarchyLevel,
                nrOfHierarchyLevels);
        return rootDirectory;
    }

    /**
     * Sets up a subhierarchy of the filesystem by generating other subdirectories
     * and simple files.
     *
     * @param rootDirectory        The root.
     * @param nrOfSubDirectories   How many subdirectories (max).
     * @param nrOfFiles            The max. number of files per directory.
     * @param actualHierarchyLevel The actual hierarchy level.
     * @param maxHierarchyLevel    The maximal hierarchy level.
     */
    public void setupFileHierarchy(CustomDirectory rootDirectory, int nrOfSubDirectories, int nrOfFiles,
                                   int actualHierarchyLevel, int maxHierarchyLevel) {

        if (actualHierarchyLevel < maxHierarchyLevel) {
            for (int i = 0; i < nrOfSubDirectories; i++) {
                CustomDirectory generatedSubDir = this.createCustomDirectory(DIR_SUFFIX + "_" + i, rootDirectory);
                rootDirectory.addDirectory(generatedSubDir);
                generatedSubDir.setOwner(this.getRandomPerson());
                this.addRandomPermissions(generatedSubDir);
                generatedSubDir.setCreationDate(this.createRandomDate());

                this.setupFileHierarchy(generatedSubDir, nrOfSubDirectories, nrOfFiles, actualHierarchyLevel + 1,
                        maxHierarchyLevel);
            }
        }
        for (int i = 0; i < nrOfFiles; i++) {
            SimpleFile file = this.createSimpleFile(FILE_SUFFIX + "_" + i, this.getRandomFileType(), rootDirectory);
            rootDirectory.addFile(file);
            file.setOwner(this.getRandomPerson());
            file.setCreationDate(this.createRandomDate());
            this.addRandomPermissions(file);
        }
    }

}
