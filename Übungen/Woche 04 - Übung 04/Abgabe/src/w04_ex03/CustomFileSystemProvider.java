package w04_ex03;

import w04_ex02.Person;

import java.util.Collection;

/**
 * Specifies relevant methods for generating a file-system.
 *
 * @author Alexander Blaas
 */
public interface CustomFileSystemProvider {

    /**
     * Provides a file system
     *
     * @param nrOfHierarchyLevels Specifies how many hierarchy levels the file
     *                            system should have.
     * @param rootDirName         The root-dir name.
     * @return The root-directory of a file system.
     */
    public CustomDirectory provideFileSystem(int nrOfHierarchyLevels, String rootDirName);

    /**
     * Gets all available file-types (Mainly required for testing purposes).
     *
     * @return A collection of file-types.
     */
    public Collection<String> getFileTypes();

    /**
     * Gets a collection of users (owners, respectively, permitted users). It is
     * also mainly used for testing purposes.
     *
     * @return
     */
    public Collection<Person> getPersonList();

    /**
     * Execute any initialization-functionality inside this method.
     */
    public void doInit();

}