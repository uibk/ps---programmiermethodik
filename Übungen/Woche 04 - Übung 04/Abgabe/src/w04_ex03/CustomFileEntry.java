package w04_ex03;

import w04_ex02.Person;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;

/**
 * Represents a custom file entry.
 *
 * @author Alexander Blaas
 */
public abstract class CustomFileEntry implements Comparable<CustomFileEntry> {

    private String name;
    private CustomFileEntry parent;
    private Person owner;
    private Collection<Person> permittedUsers = new HashSet<>();
    private Date creationDate = new Date();

    public CustomFileEntry(String name, CustomFileEntry parent) {
        super();
        this.name = name;
        this.parent = parent;
    }

    public CustomFileEntry(String name, CustomFileEntry parent, Person owner, Date creationDate) {
        super();
        this.name = name;
        this.parent = parent;
        this.owner = owner;
        this.creationDate = creationDate;
    }

    /**
     * Constructs the path by recursively prepending the files' parent path.
     *
     * @return The whole file-path.
     */
    public String getPath() {
        StringBuilder builder = new StringBuilder();

        if (this.parent != null) {
            builder.insert(0, this.parent.getPath());
        }
        builder.append(this.name);
        return builder.toString();
    }

    /**
     * Gets the file type (or "directory" if it is a directory).
     *
     * @return The file type (e.g. txt, etc...).
     */
    public abstract String getFileType();

    /**
     * Adds a user to the list of permitted users.
     *
     * @param user The user to grant permission.
     */
    public void addPermission(Person user) {
        this.permittedUsers.add(user);
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the parent
     */
    public CustomFileEntry getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(CustomFileEntry parent) {
        this.parent = parent;
    }

    /**
     * @return the owner
     */
    public Person getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(Person owner) {
        this.owner = owner;
    }

    /**
     * @return the permittedUsers
     */
    public Collection<Person> getPermittedUsers() {
        return Collections.unmodifiableCollection(permittedUsers);
    }

    /**
     * @param permittedUsers the permittedUsers to set
     */
    public void setPermittedUsers(Collection<Person> permittedUsers) {
        this.permittedUsers = permittedUsers;
    }

    /**
     * @return the creationDate
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Required for storing CustomFileEntries into a treeMap.
     */
    @Override
    public int compareTo(CustomFileEntry o) {
        return this.getPath().compareTo(o.getPath());
    }

    /**
     * Creates a string of all relevant informations within a
     * CustomFileEntry-Object.
     */
    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        StringBuilder permittedPersons = new StringBuilder();
        for (Person person : this.permittedUsers) {
            permittedPersons.append(person);
            permittedPersons.append(", ");
        }

        String permissionsAsString;

        if (permittedPersons.length() > 2) {
            permissionsAsString = permittedPersons.substring(0, permittedPersons.length() - 2);
        } else {
            permissionsAsString = "";
        }

        return "[path='" + this.getPath() + "', owner=" + this.owner + ", creationDate="
                + dateFormat.format(this.creationDate) + ", permissions={" + permissionsAsString + "}]";
    }

}
