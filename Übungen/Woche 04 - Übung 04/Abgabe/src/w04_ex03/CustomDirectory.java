package w04_ex03;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

/**
 * This class represents a custom directory within our file-system.
 *
 * @author Alexander Blaas
 */
public class CustomDirectory extends CustomFileEntry {

    /*
     * A directory can either hold other directories or simple files (e.g. file.txt)
     */
    private Collection<CustomDirectory> directories = new LinkedList<>();
    private Collection<SimpleFile> simpleFiles = new LinkedList<>();

    public CustomDirectory(String name, CustomFileEntry parent) {
        super(name, parent);
    }

    @Override
    public String getPath() {
        return super.getPath() + "/";
    }

    /**
     * Adds a directory to the container.
     *
     * @param dir The directory to add.
     */
    public void addDirectory(CustomDirectory dir) {
        this.directories.add(dir);
    }

    /**
     * Adds a simple file to the container.
     *
     * @param file The file to add.
     */
    public void addFile(SimpleFile file) {
        this.simpleFiles.add(file);
    }

    /**
     * Returns all the content of a given directory: Iterates recursively over all
     * subdirectories and also collects all the simple files.
     *
     * @return All subfiles of the current directory.
     */
    public Collection<CustomFileEntry> getAllContent() {
        Collection<CustomFileEntry> allContent = new LinkedList<>();

        // don't forget the root
        if (super.getParent() == null) {
            allContent.add(this);
        }

        for (CustomDirectory dir : this.directories) {
            allContent.add(dir);
            allContent.addAll(dir.getAllContent());
        }

        allContent.addAll(this.simpleFiles);
        return allContent;
    }

    /**
     * @return the directories
     */
    public Collection<CustomDirectory> getDirectories() {
        return Collections.unmodifiableCollection(directories);
    }

    /**
     * @param directories the directories to set
     */
    public void setDirectories(Collection<CustomDirectory> directories) {
        this.directories = directories;
    }

    /**
     * @return the simpleFiles
     */
    public Collection<SimpleFile> getSimpleFiles() {
        return Collections.unmodifiableCollection(simpleFiles);
    }

    /**
     * @param simpleFiles the simpleFiles to set
     */
    public void setSimpleFiles(Collection<SimpleFile> simpleFiles) {
        this.simpleFiles = simpleFiles;
    }

    /**
     * The filetype for directories
     */
    @Override
    public String getFileType() {
        return "directory";
    }

}
