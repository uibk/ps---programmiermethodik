package w04_ex03;

import w04_ex02.Person;

import java.util.Collection;
import java.util.Iterator;

/**
 * Just a couple of simple tests to test the implementation.
 *
 * @author Alexander Blaas
 */
public class FileSystemTest {

    public static final int MAX_NR_OF_PERSONS = 30;
    public static final int MAX_NR_OF_SUB_DIRECTORIES = 3;
    public static final int MAX_NR_OF_FILES_PER_DIRECTORY = 3;
    public static final int NR_OF_HIERARCHIES = 3;
    // Configuration here
    private static final String ROOT_DIR_NAME = "root";

    /**
     * Self explanatory.
     *
     * @param nanoSecStart
     * @param nanoSecEnd
     * @return
     */
    public static double calcMs(long nanoSecStart, long nanoSecEnd) {
        return (double) (nanoSecEnd - nanoSecStart) / 1000000;
    }

    /**
     * Self explanatory.
     *
     * @param files
     */
    public static void printFiles(Collection<CustomFileEntry> files) {
        Iterator<CustomFileEntry> fileIterator = files.iterator();
        while (fileIterator.hasNext()) {
            System.out.println(fileIterator.next());
        }
    }

    public static void main(String[] args) {
        CustomFileSystemProvider dataProvider = new RandomFileSystemProvider(MAX_NR_OF_PERSONS, MAX_NR_OF_SUB_DIRECTORIES, MAX_NR_OF_SUB_DIRECTORIES);
        dataProvider.doInit();

        CustomDirectory randomRootDir = dataProvider.provideFileSystem(NR_OF_HIERARCHIES, ROOT_DIR_NAME);

        FileIndex fileIndex = new PerformantFileIndex(randomRootDir);
        CustomFileSystemManager fileSystemManager = new IndexedFileSystemManager(fileIndex);

        Collection<String> fileTypes = dataProvider.getFileTypes();
        Collection<Person> randomPersons = dataProvider.getPersonList();

        long startIndexing = System.nanoTime();
        fileSystemManager.doInit();
        long stopIndexing = System.nanoTime();

        System.out.println("Indexing finished after " + calcMs(startIndexing, stopIndexing) + " ms");

        System.out.println("------------------------------");
        System.out.println("All files by path: \n");
        printFiles(fileSystemManager.sortFilesByPath(randomRootDir.getAllContent()));
        System.out.println("------------------------------\n");

        System.out.println("All files by creationDate: \n");
        printFiles(fileSystemManager.sortFilesByCreationDate(randomRootDir.getAllContent()));
        System.out.println("------------------------------\n");

        Iterator<String> fileTypeIterator = fileTypes.iterator();
        String randomFileType = fileTypeIterator.next();
        System.out.println("Files of type: " + randomFileType + "\n");

        Collection<CustomFileEntry> filesByType = fileSystemManager.findFilesByType(randomFileType);
        printFiles(filesByType);
        System.out.println("------------------------------\n");

        Iterator<Person> personIterator = randomPersons.iterator();
        Person randomOwner = personIterator.next();
        System.out.println("Files of user: " + randomOwner + "\n");

        Collection<CustomFileEntry> filesByOwner = fileSystemManager.getFilesByOwner(randomOwner);
        printFiles(filesByOwner);
        System.out.println("------------------------------\n");

        String path = "root/file_0.txt";
        System.out.print("File exists: '" + path + "': ");
        CustomFileEntry foundFile = fileSystemManager.findFile(path);
        System.out.println(fileSystemManager.fileExists(path) + (foundFile == null ? "" : " => " + foundFile));
        System.out.println("------------------------------\n");

        Person randomPermittedUser = personIterator.next();
        System.out.println("Permitted files of user: " + randomPermittedUser + "\n");

        Collection<CustomFileEntry> permittedFiles = fileSystemManager.getPermittedFiles(randomPermittedUser);
        printFiles(permittedFiles);
        System.out.println("------------------------------\n");

        String nextFileType = fileTypeIterator.next();
        System.out.println("Permitted '" + nextFileType + "'-files of user: " + randomPermittedUser + "\n");

        Collection<CustomFileEntry> permittedFilesOfType = fileSystemManager
                .getPermittedFilesOfType(randomPermittedUser, nextFileType);
        printFiles(permittedFilesOfType);
        System.out.println("------------------------------\n");
    }

}
