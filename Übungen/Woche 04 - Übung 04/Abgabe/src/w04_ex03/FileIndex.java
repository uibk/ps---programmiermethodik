package w04_ex03;

/**
 * The base class for the fileIndex to implement. It implements
 * CustomFileSystemManager because all the specified methods can be performed on
 * this index.
 *
 * @author Alexander Blaas
 */
public abstract class FileIndex implements CustomFileSystemManager {

    private boolean filesAlreadyIndexed = false;
    private CustomDirectory rootFile;

    public FileIndex(CustomDirectory rootFile) {
        this.rootFile = rootFile;
    }

    /*
     * Setup the index if it doesn't exists.
     */
    @Override
    public void doInit() {
        this.createIndexIfNotExists();
    }

    /**
     * Creates the index on all subfiles of the given root-directory.
     *
     * @param rootFile The root-directory.
     */
    private void createIndex() {
        for (CustomFileEntry fileToIndex : this.rootFile.getAllContent()) {
            this.addFileToIndex(fileToIndex);
        }
    }

    /**
     * This index-initialization is a costly operation, hence do it only once.
     */
    public void createIndexIfNotExists() {
        if (!this.filesAlreadyIndexed) {
            this.createIndex();
            this.filesAlreadyIndexed = true;
        }
    }

    /**
     * This method adds every file from the file-system to the index.
     *
     * @param entry The fileEntry to index.
     */
    protected abstract void addFileToIndex(CustomFileEntry entry);
}
