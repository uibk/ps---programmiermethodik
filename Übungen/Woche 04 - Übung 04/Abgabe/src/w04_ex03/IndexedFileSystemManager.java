package w04_ex03;

import w04_ex02.Person;

import java.util.Collection;

/**
 * An implementation of CustomFileSystemManager which uses an index to speed up
 * retrieval methods.
 *
 * @author Alexander Blaas
 */
public class IndexedFileSystemManager implements CustomFileSystemManager {

    private FileIndex fileIndex;

    public IndexedFileSystemManager(FileIndex fileIndex) {
        super();
        this.fileIndex = fileIndex;
    }

    @Override
    public void doInit() {
        this.fileIndex.doInit();
    }

    @Override
    public boolean fileExists(String path) {
        this.fileIndex.createIndexIfNotExists();
        return this.fileIndex.fileExists(path);
    }

    @Override
    public CustomFileEntry findFile(String path) {
        this.fileIndex.createIndexIfNotExists();
        return this.fileIndex.findFile(path);
    }

    @Override
    public Collection<CustomFileEntry> findFilesByType(String fileType) {
        this.fileIndex.createIndexIfNotExists();
        return this.fileIndex.findFilesByType(fileType);
    }

    @Override
    public Collection<CustomFileEntry> getFilesByOwner(Person owner) {
        this.fileIndex.createIndexIfNotExists();
        return this.fileIndex.getFilesByOwner(owner);
    }

    @Override
    public Collection<CustomFileEntry> getPermittedFiles(Person user) {
        this.fileIndex.createIndexIfNotExists();
        return this.fileIndex.getPermittedFiles(user);
    }

    @Override
    public Collection<CustomFileEntry> getPermittedFilesOfType(Person user, String fileType) {
        this.fileIndex.createIndexIfNotExists();
        return this.fileIndex.getPermittedFilesOfType(user, fileType);
    }

    @Override
    public Collection<CustomFileEntry> sortFilesByPath(Collection<CustomFileEntry> toSort) {
        this.fileIndex.createIndexIfNotExists();
        return this.fileIndex.sortFilesByPath(toSort);
    }

    @Override
    public Collection<CustomFileEntry> sortFilesByCreationDate(Collection<CustomFileEntry> toSort) {
        this.fileIndex.createIndexIfNotExists();
        return this.fileIndex.sortFilesByCreationDate(toSort);
    }

}
