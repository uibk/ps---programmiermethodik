# Week 03 Solutions #

## Exercise 1: Proof by Contradiction ##

Prove by contradiction that $2²^n\notin O(2^n)$

With contradiction we can define our proposition that $2^n\in O(2²^n)$

The Big-O Definition says that for $2^n$ $\le$ $c2²^n$ for any $c > 0$ and $n0 \ge 1$

When we choose $n0 = 5$ we get: $2^5$ $\le$ $c2²^*⁵$ which results in $2⁵ = 32 \le c 2² * 2⁵ = 4 * 32 = 128$ so $32 \le 128$.

$c$ can then be state as $c = 1$ to prove the statement that $32 \le 1 * 128$.

As we know that an asymptotic growth rate is element of any higher growth rate:

$O(2^n) \subset O(2^{n+1}) \subset O(2^{n+2}) \subset O(2^{2n})$

we have proven that $2²^n\notin O(2^n)$ but $2^n\in O(2²^n)$

## Exercise 2: Proof by Induction ##

Prove by induction that the following is true:

$$f(n)=\sum_{i=1}^{n} i^{3} = \frac{n²(n+1)²}{4}$$

The start of the induction is to set $n = 0$ which would be an empty sum:

$$ 0^{3} = \frac{0²(0+1)²}{4} = 0$$

We have proven that the formular with $n = 0$ is true. The definition of the induction says that a formula for every $n = 0$ as well as for any $n + 1$ must be true.

$$\sum_{i=1}^{n+1} i^{3} = \frac{n²(n+1)²}{4} + (n+1)³ $$

The next step is to use the binomial theorem to transfer:
$$\frac{(n+1)²(n+2)²}{4} + (n+1)³ $$

into:

$$\frac{(n²+2n+1)(n²+4n+4)}{4} + (n³+3n²+3n+1) $$

we simplify our solution to:

$$\frac{(n⁴+4n³+4n²+2n³+8n²+8n+n²+4n+4)}{4} $$

These terms summed up concludes to:
$${(n⁴+6n³+13n²+12n+4)} $$

## Exercise 3: Code Analysis ##

Determine the Big-O complexity of the following code fragment:

```Java
public static int example(int N) {
    int counter = 0;
    for(int i=2; i<=N; i=i*2) {
        for(int j=i; j<=N; j=j*2) {
            counter++;
        }
    }
    return counter;
}
```

## Exercise 4: Recursion ##

```Java
public class RecursionApplication {
    public static void main(String[] args) {
        decimalToBinary(25);
    }

    private static void decimalToBinary(int value) {
        if (value > 0) {
            decimalToBinary(value / 2);
            System.out.print(value % 2);
        }
    }
}

Output in console:
11001
