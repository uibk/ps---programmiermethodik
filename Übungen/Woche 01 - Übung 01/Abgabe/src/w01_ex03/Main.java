package w01_ex03;

public class Main {
    public static void main(String[] args) {
        Person john = new Person("John", "Doe");
        john.getFullName();
        Person donald = new Person("Donald", "Duck");
        donald.getFullName();

        john.greet(donald);
    }
}
