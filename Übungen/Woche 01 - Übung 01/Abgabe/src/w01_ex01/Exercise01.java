package w01_ex01;

public class Exercise01 {

    public static void main(String[] args) {
        System.out.println(6 * 5 / 3);
        System.out.println(1 << 8 % 3);
        System.out.println((short) Integer.MAX_VALUE);
        System.out.println(23 / (double) 11);
        System.out.println((double) (23 / 11));
        System.out.println(42f);
        System.out.println(4e3D);
        System.out.println((11 * 1.2) != 47);
//        System.out.println("Peter=Coffee+" + 'chocolate' + 2.0);
        System.out.println("Peter=Coffee+" + "chocolate" + 2.0);
        System.out.println(1 == 24 % 3 && 4 > 7 || true);
        System.out.println(1 == 24 % 3 ? 4 : 7);
    }
}
