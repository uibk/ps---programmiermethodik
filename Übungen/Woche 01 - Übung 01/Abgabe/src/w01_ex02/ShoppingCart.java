package w01_ex02;

/**
 * This class is used for calculating a possible discount for a shopping cart.
 */
public class ShoppingCart {

    /**
     * Main method of the program.
     *
     * @param args command line arguments.
     */
    public static void main(final String[] args) {

        // list of product prices in shopping cart as a Java array

        // shopping cart 1
        double[] shoppingCart1 =
                new double[]{40.95, 10.80, 2.35, 22.50, 13.45, 5.95, 33.33, 41.10, 7.75, 5};

        // shopping cart 2
        double[] shoppingCart2 = new double[]{40.95, 10.80, 2.35, 22.50, 13.45, 5.95, 33.33,
                41.10, 7.75, 5, 1.25, 10.40, 6.10, 14.40, 55.55};

        // shopping cart 3
        double[] shoppingCart3 = new double[]{40.95, 10.80, 2.35, 22.50, 13.45, 5.95, 33.33,
                41.10, 7.75, 5, 1.25, 10.40, 6.10, 14.40, 55.55, 4.35, 2.02, 45.67, 35.34, 345.46, 34.30,
                66.30, 4.23, 1.23, 5.77, 85.89};

        // shopping cart 4
        int[] shoppingCart4 = new int[]{40, 10, 2, 22, 13, 5, 33, 41, 7, 5};

        double[] currentCart = shoppingCart3;

        int numberOfItems = currentCart.length;
        double originalPrice = totalPrice(currentCart);
        double discountedPrice = calculateDiscountedPrice(originalPrice, numberOfItems);

        // ******OUTPUT on console:******
        System.out.println("original price:");
        System.out.printf("%.2f", originalPrice);
        System.out.println();
        System.out.println("discounted price:");
        System.out.printf("%.2f", discountedPrice);
        // after comma
    }

    /**
     * Calculates the discounted price for a shopping cart based on the number of items in the
     * cart and the discount amount.
     *
     * @param originalPrice the price of the cart without discount.
     * @param numberOfItems the number of items in the cart.
     * @return The discounted price of the shopping cart.
     */
    private static double calculateDiscountedPrice(final double originalPrice, final int numberOfItems) {
        double discountAmount = calculateDiscount(originalPrice);
        double discount = 0.0d;

        // use switch statement to accomplish cases of discount's (5, 10, 15, 20, 25, 30 items)
        switch (numberOfItems) {
            case 5:
            case 10:
            case 15:
            case 20:
            case 25:
            case 30:
                discount = originalPrice * (discountAmount / 100) + numberOfItems;
                break;
            default:
                break;
        }
        // For some special cases one will get discount on the original price.
        //
        // One gets discount if there are exactly 5, 10, 15, 20, 25 or 30 items in
        // the cart. If this is the case then the calculation is done as follows:
        //
        // 1. calculate the discount based on the original price:
        //    priceDiscount = originalPrice * discountAmount / 100
        // 2. add the number of items (5, 10, 15, 20, 25 or 30) to that
        // priceDiscount which results in the final discount.
        //
        // You have to return the final price of the cart (stated in the javadoc).
        // Keep in mind that if you have you have to calculate a discounted price
        // that the price have to be round to two digits before it is returned.
        return Math.round(originalPrice - discount);
    }

    /**
     * Calculates the discount amount depending on original price of the cart.
     *
     * @param originalPrice the original total price of a cart.
     * @return The discount that one gets on the original price.
     */
    private static double calculateDiscount(final double originalPrice) {
        if (originalPrice < 50) {
            return 0;
        }
        if (originalPrice >= 50 && originalPrice < 60) {
            return 5;
        }

        if (originalPrice >= 60 && originalPrice < 70) {
            return 10;
        }

        if (originalPrice >= 70 && originalPrice < 100) {
            return 15;
        }

        if (originalPrice >= 100) {
            return 20;
        }
        return 0;
    }


    /**
     * Calculates the total price of all product contained in a cart.
     *
     * @param cart the shopping cart to calculate the price for.
     * @return The summ of all prices in a cart.
     */
    private static double totalPrice(final double[] cart) {
        double sum = 0.0;
        for (double price : cart) {
            sum += price;
        }

        return Math.round(sum * 100) / 100.0d;
    }
}
